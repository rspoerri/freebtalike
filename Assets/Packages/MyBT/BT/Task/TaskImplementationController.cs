﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyBT
{
    [System.Serializable]
    public class TaskImplementationController
    {

        TaskController taskController;
        /// 
        private bool readAllComponents = false;

        public TaskImplementationController(TaskController _taskController, bool __readAllComponents, bool __debugLog)
        {
            taskController = _taskController;
            readAllComponents = __readAllComponents;
        }

        public void Reset()
        {
            taskController.tasks = null;
        }

        public static readonly BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.Static | BindingFlags.InvokeMethod | BindingFlags.GetField;
        //public Dictionary<System.Type, List<Task>> componentTasks = new Dictionary<Type, List<Task>>();
        public void CheckComponents(GameObject checkObject)
        {
            taskController.tasks = new Dictionary<string, Dictionary<string, TaskImplementation>>();

            foreach (Component component in checkObject.GetComponents<Component>())
            {
                //var componentType = component.GetType();

                MemberInfo[] members = component.GetType().GetMembers(bindingFlags);
                foreach (MemberInfo memberInfo in members)
                {
                    if (memberInfo.IsDefined(typeof(Task), true))
                    {
                        TaskImplementation taskImplementation = new TaskImplementation(memberInfo, component);
                        if (taskImplementation.HasImplementation())
                        {
                            AddTaskImplementation(taskImplementation);
                        }
                    }
                    else
                    {
                        if (readAllComponents)
                        {
                            TaskImplementation taskImplementation = new TaskImplementation(memberInfo, component);
                            if (taskImplementation.HasImplementation())
                            {
                                AddTaskImplementation(taskImplementation);
                            }
                        }
                    }
                }
            }
        }

        public void AddTaskImplementation(TaskImplementation taskImplementation)
        {
            string taskName = taskImplementation.fullName;
            string paramtersAsString = taskImplementation.parameters.ToString();

            // generate unique key dictionarys
            if (!taskController.tasks.ContainsKey(taskName))
            {
                taskController.tasks[taskName] = new Dictionary<string, TaskImplementation>();
            }

            // add the list
            if (!taskController.tasks[taskName].ContainsKey(paramtersAsString))
            {
                taskController.tasks[taskName][paramtersAsString] = taskImplementation;
            }
            // we have this exact implementation already in the dict, should normally not happen, maybe if you have 2 scripts of the same type on the object?
            else
            {
                if (taskController.debugLog)
                    Debug.LogWarning("Duplicate function: " + taskName + " with parms: " + paramtersAsString);
            }
        }
    }
}