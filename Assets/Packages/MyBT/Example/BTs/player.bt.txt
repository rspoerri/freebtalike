﻿Tree("Root") {
	Composite (Sequence, IgnoreFailed, IgnoreSucceeded) {
		SimpleController.SetRandomTarget (0, 0, 50)
		Composite (Parallel, FirstSucceeded) {
			SimpleController.NearTarget (2)
			SimpleController.RotateTowards (90)
			SimpleController.MoveForward (2)
		}
    }
}