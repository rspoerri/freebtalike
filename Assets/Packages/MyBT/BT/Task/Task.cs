﻿using UnityEngine;

namespace MyBT {
    //public enum TaskRequestState {
    //	nochange,
    //	restart,
    //	stop
    //}

    // states the task can have, action can read this
    public enum TaskState {
        // 
        NotRunning,
        // the action has not been initiated before
        FirstRun,
        // it has run at least once with prepar
        Running,
        // this node has to stop in this exec, also all children need to stop
        Aborting
    }

    // result the action can return
    public enum TaskResult {
        Running,
        Succeeded,
        Failed,
        Error
    }

    /// <summary>
    /// The best solution i found so far to allow access from the Task to the TaskParameters and State is by Storing the 
    /// current action in the TaskController
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Method | System.AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class Task : System.Attribute {
        public static TaskController GetTaskController(MonoBehaviour other) {
            return other.GetComponent<TaskController>();
        }
        // get the current active node from TaskController
        public static ActionNode GetCurrentNode(MonoBehaviour other) {
            return GetTaskController(other).GetCurrentNode() as ActionNode;
        }
        public static ActionNode GetCurrentNode() {
            return GetTaskController(SingletonTaskController.Instance.current).GetCurrentNode() as ActionNode;
        }
        public static ActionNode current {
            get {
                return GetCurrentNode();
            }
        }

        public static TaskState GetState(MonoBehaviour other) {
            return GetCurrentNode(other).taskState;
        }
        public static TaskState GetState() {
            return GetState(SingletonTaskController.Instance.current);
        }
        public static TaskState getState {
            get {
                return GetState();
            }
        }

        public static TaskState status {
            get {
                return GetCurrentNode().taskState;
            }
        }


        public static bool IsRunning(MonoBehaviour other) {
            return (GetCurrentNode(other).taskState == TaskState.FirstRun || GetCurrentNode(other).taskState == TaskState.Running);
        }
        public static bool isRunning {
            get {
                return IsRunning(SingletonTaskController.Instance.current);
            }
        }

        public static bool IsStarting(MonoBehaviour other) {
            return (GetCurrentNode(other).taskState == TaskState.FirstRun);
        }
        public static bool isStarting {
            get {
                return IsStarting(SingletonTaskController.Instance.current);
            }
        }

        public static bool IsAborting(MonoBehaviour other) {
            return (GetCurrentNode(other).taskState == TaskState.Aborting);
        }
        public static bool isAborting {
            get {
                return IsAborting(SingletonTaskController.Instance.current);
            }
        }

        public static bool IsDebugging(MonoBehaviour other) {
            return (GetCurrentNode(other).logActive);
        }
        public static bool isDebugging {
            get {
                return IsDebugging(SingletonTaskController.Instance.current);
            }
        }


        public static string GetLog(MonoBehaviour other) {
            return GetCurrentNode(other).logString;
        }
        public static void SetLog(MonoBehaviour other, string value) {
            GetCurrentNode(other).logString = value;
        }
        public static string GetLog() {
            return GetLog(SingletonTaskController.Instance.current);
        }
        public static void SetLog(string value) {
            SetLog(SingletonTaskController.Instance.current, value);
        }
        public static string log {
            get {
                return GetLog();
            }
            set {
                SetLog(value);
            }
        }

        public static object GetData(MonoBehaviour other) {
            return GetCurrentNode(other).userData;
        }
        public static object GetData() {
            return GetData(SingletonTaskController.Instance.current);
        }

        public static void SetData(MonoBehaviour other, object value) {
            GetCurrentNode(other).userData = value;
        }
        public static void SetData(object value) {
            SetData(SingletonTaskController.Instance.current, value);
        }

        public static object data {
            get {
                return GetData();
            }
            set {
                SetData(value);
            }
        }


        public static void SetFailed(MonoBehaviour other) {
            GetTaskController(other).SetUiUpdate();
            GetCurrentNode(other).taskResult = TaskResult.Failed;
        }
        public static void SetFailed() {
            SetFailed(SingletonTaskController.Instance.current);
        }

        public static void SetSucceeded(MonoBehaviour other) {
            GetTaskController(other).SetUiUpdate();
            GetCurrentNode(other).taskResult = TaskResult.Succeeded;
        }
        public static void SetSucceeded() {
            SetSucceeded(SingletonTaskController.Instance.current);
        }
        public static void SetSucceed() {
            SetSucceeded(SingletonTaskController.Instance.current);
        }
    }
}