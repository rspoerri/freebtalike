﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MyBT {
    public enum CompositeParameter {
        Undefined,

        // order of execution
        Sequence,
        Selector,
        Race,
        Marathon,
        //Randomized, // can be combined with sequence

        // 
        //Loop, // only useful with sequence

        // dont reset index when finished
        // continue where we left the composite before
        //Continue,

        // abort condictions
        //AllSucceeded,
        //AnySucceeded,
        //FirstSucceeded,
        //IgnoreSucceeded,

        //AllFailed,
        //AnyFailed,
        //FirstFailed,
        //IgnoreFailed

        // abort condictions
        //AbortSelf,
        //AbortChildren,
        //AbortAny,
    }


    public class CompositeNode : Node {
        public List<CompositeParameter> parameters;
        public int activeNodeId = 0;

        private NodeResult nodeResult;
        private NodeResult[] nodeResults;

        private CompositeParameter compositeType = CompositeParameter.Undefined;

        public bool Has(CompositeParameter compositeParameter) {
            return parameters.Contains(compositeParameter);
        }

        public CompositeNode(TaskController _taskController, int _nodeDepth, int[] _tokenIndexes, List<string> stringParameter) : base(_taskController, _nodeDepth, _tokenIndexes) {
            parameters = new List<CompositeParameter>();
            for (int i = 0; i < stringParameter.Count; i++) {
                CompositeParameter compositeParameter = CompositeParameter.Undefined;
                if (Enum.TryParse(stringParameter[i], out compositeParameter)) {
                    parameters.Add(compositeParameter);
                }

                else {
                    //UnityEngine.Debug.LogError("CompositeNode." + this + " cannot convert parameter " + stringParameter[i]);
                    throw new Exception("CompositeNode." + this + " cannot convert parameter " + stringParameter[i]);
                }
            }

            if (Has(CompositeParameter.Race)) {
                compositeType = CompositeParameter.Race;
            }
            else if (Has(CompositeParameter.Marathon)) {
                compositeType = CompositeParameter.Marathon;
            }
            else if (Has(CompositeParameter.Sequence)) {
                compositeType = CompositeParameter.Sequence;
            }
            else if (Has(CompositeParameter.Selector)) {
                compositeType = CompositeParameter.Selector;
            }
        }

        /// <summary>
        /// reset the whole behaviour tree structure
        /// </summary>
        public override void Reset() {
            base.Reset();
            parameters = null;
            activeNodeId = 0;
        }

        public override NodeResult Tick(NodeExecute nodeExecute, int recursions) {
            base.PreTick(nodeExecute);

            if (debugActive) {
                Debug.Log(NodeLogger("Execute Before", "ResultCache " + nodeResult + " NodeExecute " + nodeExecute + " NodeState " + nodeState + " compositeType " + compositeType, recursions));
            }

            switch (compositeType) {
                case CompositeParameter.Race:
                    nodeResult = HandleRaceExecute(nodeExecute, recursions);
                    break;
                case CompositeParameter.Marathon:
                    nodeResult = HandleMarathonExecute(nodeExecute, recursions);
                    break;
                case CompositeParameter.Sequence:
                    nodeResult = HandleSequenceExecute(nodeExecute, recursions);
                    break;
                case CompositeParameter.Selector:
                    nodeResult = HandleSelectorExecute(nodeExecute, recursions);
                    break;
                default:
                    Debug.LogError("no behaviour defined for CompositeNode, use at least Sequence or Parallel");
                    break;
            }

            if (debugActive) {
                Debug.Log(NodeLogger("Execute After", "ResultCache " + nodeResult + " NodeExecute " + nodeExecute + " NodeState " + nodeState + " compositeType " + compositeType, recursions));
            }

            base.PostTick(nodeResult);

            return nodeResult;
        }

        private void ExecuteOnAll(NodeExecute nodeExecute, int recursions) {
            nodeResults = new NodeResult[children.Count];
            for (int i = 0; i < children.Count; i++) {
                if (!(children[i] is CommentNode)) {
                    nodeResults[i] = children[i].Tick(nodeExecute, recursions + 1);
                }
            }
        }

        /// <summary>
        /// Race Node
        /// 
        /// Starts all Children in parallel:
        /// - any child running -> continue in loop
        /// - any child success -> return success, abort all remaining running
        /// - any child failed -> continue in loop
        /// - return after 1 iteration
        /// - if all fail, fail.
        /// </summary>
        private NodeResult HandleRaceExecute(NodeExecute nodeExecute, int recursions) {
            switch (nodeState) {
                case NodeState.NotRunning:
                    // dont do anything
                    break;

                case NodeState.FirstRun:
                    nodeResults = new NodeResult[children.Count];
                    for (int i=0; i<children.Count; i++) {
                        nodeResults[i] = NodeResult.Running;
                    }
                    goto case NodeState.Running;

                case NodeState.Running:
                    for (int i = 0; i < children.Count; i++) {
                        if (!(children[i] is CommentNode)) {
                            // if still in running state
                            if (nodeResults[i] == NodeResult.Running) { 
                                nodeResults[i] = children[i].Tick(nodeExecute, recursions + 1);
                            }
                        }
                        // set comment nodes as failed
                        else {
                            nodeResults[i] = NodeResult.Failed;
                        }
                    }

                    //ExecuteOnAll(NodeExecute.Run, recursions); // writes to taskResults
                    bool hasSucceeded = false;
                    bool allFailed = true;
                    for (int i=0; i< nodeResults.Length; i++) {
                        if (nodeResults[i] == NodeResult.Succeeded) {
                            hasSucceeded = true;
                        }
                        if (nodeResults[i] != NodeResult.Failed) {
                            allFailed = false;
                        }
                    }
                    if (hasSucceeded) {
                        Tick(NodeExecute.Abort, recursions + 1);
                        return NodeResult.Succeeded;
                    }
                    if (allFailed) {
                        return NodeResult.Failed;
                    }
                    return NodeResult.Running;
                    
                case NodeState.Aborting:
                    ExecuteOnAll(NodeExecute.Abort, recursions); // writes to taskResults
                    return NodeResult.Running;
            }

            Debug.LogError("should not happen");
            return NodeResult.Error;
        }

        /// <summary>
        /// Marathon Node
        /// 
        /// Starts all Children in parallel:
        /// - any child running -> continue in loop
        /// - any child success -> continue in loop
        /// - any child failed -> continue in loop
        /// - return after 1 iteration
        /// - succeed when all are succeeded or failed
        /// </summary>
        private NodeResult HandleMarathonExecute(NodeExecute nodeExecute, int recursions) {
            switch (nodeState) {
                case NodeState.NotRunning:
                    // dont do anything
                    Debug.LogWarning("HandleMarathonExecute in NotRunning State");
                    break;

                case NodeState.FirstRun:
                    nodeResults = new NodeResult[children.Count];
                    for (int i = 0; i < children.Count; i++) {
                        nodeResults[i] = NodeResult.Running;
                    }
                    goto case NodeState.Running;

                case NodeState.Running:
                    for (int i = 0; i < children.Count; i++) {
                        if (!(children[i] is CommentNode)) {
                            // if still in running state
                            if (nodeResults[i] == NodeResult.Running) {
                                nodeResults[i] = children[i].Tick(nodeExecute, recursions + 1);
                            }
                        }
                        // set comment nodes as succeeded
                        else {
                            nodeResults[i] = NodeResult.Succeeded;
                        }
                    }

                    bool allSucceededOrFailed = true;
                    for (int i = 0; i < nodeResults.Length; i++) {
                        if ((nodeResults[i] == NodeResult.Running) || (nodeResults[i] == NodeResult.Error)) {
                            allSucceededOrFailed = false;
                        }
                        //if ((nodeResults[i] != NodeResult.Succeeded) || (nodeResults[i] != NodeResult.Failed)) {
                        //    allSucceededOrFailed = false;
                        //}
                    }
                    if (allSucceededOrFailed) {
                        return NodeResult.Succeeded;
                    }
                    return NodeResult.Running;
                    
                case NodeState.Aborting:
                    ExecuteOnAll(NodeExecute.Abort, recursions); // writes to taskResults
                    return NodeResult.Running;
            }

            //Debug.LogError("should not happen");
            return NodeResult.Error;
        }

        /// <summary>
        /// Sequence node
        /// 
        /// Starts all Children in sequence:
        /// - any child running -> return running
        /// - any child success -> continue in loop 
        /// - any child failed -> return failed
        /// - finally -> exit success (there is no looping)
        ///
        /// Figure II.Graphical representation of a sequence composition of N tasks.
        /// Sequence nodes are used to find and execute the first child that has not yet succeeded. A sequence node will return immediately with a status code of failure or running when one of its children returns failure or running (see Figure II and the pseudocode below). The children are ticked in order, from left to right.
        ///
        /// In pseudocode, the algorithm for a sequence composition is:
        /// 1 for i from 1 to n do
        /// 2     childstatus ← Tick(child(i))
        /// 3     if childstatus = running
        /// 4        return running
        /// 5     else if childstatus = failure
        /// 6        return failure
        /// 7 end
        /// 8 return success
        /// </summary>
        private int sequenceIndex = -1;
        private NodeResult HandleSequenceExecute(NodeExecute nodeExecute, int recursions) {
            switch (nodeState) {
                case NodeState.NotRunning:
                    // dont do anything
                    break;

                case NodeState.FirstRun:
                    sequenceIndex = 0;
                    goto case NodeState.Running;
                case NodeState.Running:
                    while (sequenceIndex < children.Count) {
                        if (children[sequenceIndex] is CommentNode) {
                            // skip
                        }
                        else {
                            nodeResult = children[sequenceIndex].Tick(nodeExecute, recursions + 1);

                            if (nodeResult == NodeResult.Running)
                                return NodeResult.Running;
                            if (nodeResult == NodeResult.Failed)
                                return NodeResult.Failed;
                        }
                        sequenceIndex += 1;
                    }

                    return NodeResult.Succeeded;

                case NodeState.Aborting:
                    ExecuteOnAll(NodeExecute.Abort, recursions); // writes to taskResults
                    return NodeResult.Running;
            }

            // Debug.LogWarning("should not happen");
            return NodeResult.Failed;
        }

        /// <summary>
        /// Selector (fallback) node
        /// 
        /// Starts all Children in sequence:
        /// - any child running -> continue in loop
        /// - any child success -> return success
        /// - any child failed -> continue in loop 
        /// - finally -> exit failed (there is no looping)
        ///
        /// Fallback nodes are used to find and execute the first child that does not fail. A fallback node will return immediately with a status code of success or running when one of its children returns success or running (see Figure I and the pseudocode below). The children are ticked in order of importance, from left to right.
        /// 
        /// In pseudocode, the algorithm for a fallback composition is:
        /// 1 for i from 1 to n do
        /// 2     childstatus ← Tick(child(i))
        /// 3     if childstatus = running
        /// 4        return running
        /// 5     else if childstatus = success
        /// 6        return success
        /// 7 end
        /// 8 return failure
        /// </summary>
        private int selectorIndex = -1;
        private NodeResult HandleSelectorExecute(NodeExecute nodeExecute, int recursions) {
            switch (nodeState) {
                case NodeState.NotRunning:
                    // dont do anything
                    break;
                case NodeState.FirstRun:
                    selectorIndex = 0;
                    goto case NodeState.Running;
                case NodeState.Running:
                    while (selectorIndex < children.Count) {
                        if (children[selectorIndex] is CommentNode) {
                            // skip
                        }
                        else {
                            nodeResult = children[selectorIndex].Tick(nodeExecute, recursions + 1);

                            if (nodeResult == NodeResult.Running)
                                return NodeResult.Running;
                            if (nodeResult == NodeResult.Succeeded)
                                return NodeResult.Succeeded;
                        }
                        selectorIndex += 1;
                    }

                    return NodeResult.Failed;

                case NodeState.Aborting:
                    ExecuteOnAll(NodeExecute.Abort, recursions); // writes to taskResults
                    return NodeResult.Running;
            }

            Debug.LogError("should not happen");
            return NodeResult.Error;
        }
    }
}