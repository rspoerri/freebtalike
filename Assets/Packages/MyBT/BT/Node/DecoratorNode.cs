﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MyBT
{
    public enum Decorators
    {
        // https://legacy.opsive.com/assets/BehaviorDesigner/documentation.php?id=34
        ConditionalEvaluator,
        Interrupt,
        Inverter,
        Repeater,
        ReturnFailure,
        ReturnSuccess,
        TaskGuard,
        UntilFailure,
        UntilSuccess
    }

    public class DecoratorNode : Node
    {
        public DecoratorNode(TaskController _taskController, int _nodeDepth, int[] _tokenIndexes) : base(_taskController, _nodeDepth, _tokenIndexes)
        {
            // TODO!
        }
    }
}