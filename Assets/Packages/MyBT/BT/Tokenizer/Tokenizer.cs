﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace MyBT
{
    public enum SyntaxLabels {
        // math operators
        Plus,
        Minus,
        SingleLineCommentStart,
        MultiLineCommentStart,
        MultiLineCommentEnd,
        Equal,

        EOL,
        QuoteToken,
        Colon,
        Semicolon,
        Comma,
        Questionmark,
        Whitespace,

        // generic functions and variable tokens
        Variable,
        Function,

        // behaviour tree tokens
        TreeNameKeyword,
        RunTreeNameKeyword,
        CompositeNameKeyword,
        ActionNameKeyword,
        DecoratorNameKeyword,

        // Literal Values
        TrueKeyword,
        FalseKeyword,

        NumberSymbol,
        CharSymbol,
        StringSymbol,

        OpenNormalBracket,
        CloseNormalBracket,

        OpenCurlyBracket,
        CloseCurlyBracket,

        OpenSquareBracket,
        CloseSquareBracket,
    }

    public enum PatternLabels {
        CharPattern,
        StringPattern,
        NumberCharPattern,
        NumberPattern,
        WhitespacePattern,
        NewlinePattern
    }



    public class Tokenizer {
        TaskController taskController;

        int cursor;
        int line = 0, col = 0;

        private static readonly Dictionary<string, SyntaxLabels> keywords = new Dictionary<string, SyntaxLabels>() {
            {"True",        SyntaxLabels.TrueKeyword            },
            {"False",       SyntaxLabels.FalseKeyword           },

            {"Tree",        SyntaxLabels.TreeNameKeyword        },
            {"Composite",   SyntaxLabels.CompositeNameKeyword   },
            {"Action",      SyntaxLabels.ActionNameKeyword      },
            {"Decorator",   SyntaxLabels.DecoratorNameKeyword   },
            {"RunTree",     SyntaxLabels.RunTreeNameKeyword     }
        };

        private static readonly Dictionary<PatternLabels, Regex> patterns = new Dictionary<PatternLabels, Regex>() {
            {PatternLabels.CharPattern,         new Regex("[\\$a-zA-Z\\.]")     },
            {PatternLabels.StringPattern,       new Regex("[^\"]")              },
            {PatternLabels.NumberPattern,       new Regex("[-0-9\\.]")          },
            {PatternLabels.NumberCharPattern,   new Regex("[-[0-9a-zA-Z\\.]")   },
            {PatternLabels.WhitespacePattern,   new Regex("[ \t]")              },
            {PatternLabels.NewlinePattern,   new Regex("[\r\n]")                }
            
        };

        private static readonly Dictionary<SyntaxLabels, Regex> Definitions = new Dictionary<SyntaxLabels, Regex>() {
            { SyntaxLabels.Colon,                   new Regex(":")       },
            { SyntaxLabels.Semicolon,               new Regex(";")       },
            { SyntaxLabels.Minus,                   new Regex("\\-")     },
            { SyntaxLabels.Plus,                    new Regex("\\+")     },

            { SyntaxLabels.Comma,                   new Regex(",")       },
            { SyntaxLabels.EOL,                     new Regex("[\\r\\n]")},
            { SyntaxLabels.Whitespace,              new Regex("\\s")     },
            { SyntaxLabels.Questionmark,            new Regex("\\?")     },
            { SyntaxLabels.SingleLineCommentStart,  new Regex("\\/\\/")  },
            { SyntaxLabels.MultiLineCommentStart,   new Regex("\\/\\*")  },
            { SyntaxLabels.MultiLineCommentEnd,     new Regex("\\*\\/")  },

            { SyntaxLabels.Equal,                   new Regex("==")      },

            { SyntaxLabels.CharSymbol,              new Regex("\'")      },
            { SyntaxLabels.StringSymbol,            new Regex("\"")      },
            { SyntaxLabels.NumberSymbol,            new Regex("[0-9-]")  },

            { SyntaxLabels.OpenCurlyBracket,        new Regex("\\{")     },
            { SyntaxLabels.CloseCurlyBracket,       new Regex("\\}")     },
            { SyntaxLabels.OpenNormalBracket,       new Regex("\\(")     },
            { SyntaxLabels.CloseNormalBracket,      new Regex("\\)")     },
            { SyntaxLabels.OpenSquareBracket,       new Regex("\\[")     },
            { SyntaxLabels.CloseSquareBracket,      new Regex("\\]")     }
        };

        static readonly SyntaxLabels[] LiteralTokens = {
            SyntaxLabels.EOL,
            SyntaxLabels.Whitespace,
            SyntaxLabels.Questionmark,
            SyntaxLabels.Colon,
            SyntaxLabels.Semicolon,
            SyntaxLabels.Comma,
            SyntaxLabels.OpenNormalBracket,
            SyntaxLabels.CloseNormalBracket,
            SyntaxLabels.OpenSquareBracket,
            SyntaxLabels.CloseSquareBracket,
            SyntaxLabels.OpenCurlyBracket,
            SyntaxLabels.CloseCurlyBracket,
            SyntaxLabels.Plus,
            SyntaxLabels.Minus
        };

        public Tokenizer(TaskController _taskController) {
            this.taskController = _taskController;
        }

        private bool PatternMatch(Regex expression, int size = 1)
                => taskController.code.Length >= cursor + size && expression.IsMatch(taskController.code.Substring(cursor, size));

        private bool SynaxMatch(SyntaxLabels kind, int size = 1)
                => PatternMatch(Definitions[kind], size);

        public void Tokenize() {
            for (cursor = 0; cursor < taskController.code.Length;) {
                int savecursor = cursor;

                if (SynaxMatch(SyntaxLabels.SingleLineCommentStart, 2)) {
                    int oldcursor = cursor;

                    cursor += 2;
                    do { cursor++; }
                    while (!SynaxMatch(SyntaxLabels.EOL));

                    MakeToken(SyntaxLabels.SingleLineCommentStart, oldcursor, cursor - oldcursor);
                    continue;
                }

                if (SynaxMatch(SyntaxLabels.MultiLineCommentStart, 2)) {
                    int oldcursor = cursor;

                    cursor += 2;
                    do { cursor++; }
                    while (!SynaxMatch(SyntaxLabels.MultiLineCommentEnd, 2));
                    cursor += 2;

                    MakeToken(SyntaxLabels.MultiLineCommentStart, oldcursor, cursor - oldcursor);
                    continue;
                }

                // this should not occur on its own
                if (SynaxMatch(SyntaxLabels.MultiLineCommentEnd, 2)) {
                    MakeToken(SyntaxLabels.MultiLineCommentEnd, cursor, 2);
                    cursor += 2;
                    continue;
                }

                if (SynaxMatch(SyntaxLabels.Equal, 2)) {
                    MakeToken(SyntaxLabels.Equal, cursor, 2);
                    cursor += 2;
                    continue;
                }

                if (SynaxMatch(SyntaxLabels.CharSymbol)) {
                    int oldcursor = cursor;

                    do { cursor++; }
                    while (PatternMatch(patterns[PatternLabels.NumberPattern]));

                    if (SynaxMatch(SyntaxLabels.CharSymbol)) {
                        MakeToken(SyntaxLabels.CharSymbol, oldcursor, cursor - oldcursor + 1);
                    } else {
                        throw new Exception($"Char {taskController.code.Substring(oldcursor, cursor - oldcursor + 1)} could not be tokenized");
                    }

                    cursor++;
                    continue;
                }

                if (SynaxMatch(SyntaxLabels.StringSymbol)) {
                    int oldcursor = cursor;

                    do { cursor++; }
                    while (PatternMatch(patterns[PatternLabels.StringPattern]));

                    if (SynaxMatch(SyntaxLabels.StringSymbol)) {
                        MakeToken(SyntaxLabels.StringSymbol, oldcursor, cursor - oldcursor + 1);
                    } else {
                        throw new Exception($"String {taskController.code.Substring(oldcursor, cursor - oldcursor + 1)} could not be tokenized");
                    }
                    cursor++;
                    continue;
                }

                if (SynaxMatch(SyntaxLabels.NumberSymbol)) {
                    int oldcursor = cursor;

                    do { cursor++; }
                    while (PatternMatch(patterns[PatternLabels.NumberPattern]));

                    MakeToken(SyntaxLabels.NumberSymbol, oldcursor, cursor - oldcursor);
                    continue;
                }

                if (PatternMatch(patterns[PatternLabels.CharPattern])) {
                    int oldcursor = cursor;

                    do { cursor++; }
                    while (PatternMatch(patterns[PatternLabels.NumberCharPattern]));

                    int len = cursor - oldcursor;
                    string TokenString = taskController.code.Substring(oldcursor, len);

                    if (keywords.ContainsKey(TokenString)) {
                        MakeToken(keywords[TokenString], oldcursor, len);
                    } else {
                        while (PatternMatch(patterns[PatternLabels.WhitespacePattern])) {
                            cursor++;
                        }
                        char nextchar = taskController.code[cursor];
                        if (nextchar == '(') {
                            MakeToken(SyntaxLabels.Function, oldcursor, len);
                        } else {
                            MakeToken(SyntaxLabels.Variable, oldcursor, len);
                        }
                    }
                    continue;
                }

                if (LiteralTokens.Any(n => SynaxMatch(n))) {
                    MakeToken(LiteralTokens.First(n => SynaxMatch(n)), cursor, 1);
                    cursor++;
                    continue;
                }

                //else {
                //    throw new Exception($"CountNotTokenizeCharException char: {taskController.code[cursor]} could not be tokenized {cursor}");
                //}

                if (savecursor == cursor) { 
                    throw new Exception($"CountNotTokenizeCharException char: {taskController.code[cursor]} unknown error {cursor}");
                }

                cursor++;
            }
        }

        public string AsCode() {
            return string.Join("", taskController.tokens.Select(n => n.GetValue()));
        }

        private void MakeToken(SyntaxLabels label, int pos, int len) {
            col += len;
            if (label == SyntaxLabels.EOL) {
                line++;
                col = 0;
            }
            taskController.tokens.Add(new Token(taskController, label, pos, len, line, col));
        }
    }
}