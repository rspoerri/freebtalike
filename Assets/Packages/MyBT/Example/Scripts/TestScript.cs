﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MyBT;

namespace MyBTtTesting { 
    public enum SampleEnum {
        value0,
        value1
    }

    [Task]
    public class TestScript : MonoBehaviour {
	    public string classInstanceIdentifier;
	    private bool debug = false;

	    [Task]
        public void StringFunction(string s) {
		    if (debug)
			    Debug.Log("TestScript.StringFunction: " + classInstanceIdentifier + " " + Task.GetState(this) + " " + s);
            Task.SetSucceeded(this);
        }

	    [Task]
        public void FloatFunction(float f) {
		    if (debug)
			    Debug.Log("TestScript.FloatFunction: " + classInstanceIdentifier + " " + Task.GetState(this) + " " + f);
            Task.SetSucceeded(this);
        }

	    [Task]
	    public void IntFunction(int i) {
		    if (debug)
			    Debug.Log("TestScript.IntFunction: " + classInstanceIdentifier + " " + Task.GetState(this) + " " + i);
            Task.SetSucceeded(this);
        }

	    [Task]
        public void BoolFunction(bool b) {
		    if (debug)
			    Debug.Log("TestScript.BoolFunction: " + classInstanceIdentifier + " " + Task.GetState(this) + " " + b);
            Task.SetSucceeded(this);
        }

	    [Task]
	    public void EnumFunction(SampleEnum e) {
		    if (debug)
			    Debug.Log("TestScript.EnumFunction: " + classInstanceIdentifier + " " + Task.GetState(this) + " " + e);
            Task.SetSucceeded(this);
        }

	    [Task]
        public void PublicFunction() {
		    if (debug)
        	    Debug.Log("TestScript.PublicEnumFunction: " + classInstanceIdentifier + " " + Task.GetState(this));
            Task.SetSucceeded(this);
        }

	    [Task]
        private void PrivateFunction() {
		    if (debug)
			    Debug.Log("TestScript.PrivateFunction: " + classInstanceIdentifier + " " + Task.GetState(this));
            Task.SetSucceeded(this);
        }

	    [Task]
        public void OverloadedFunction(string s) {
		    if (debug)
			    Debug.Log("TestScript.OverloadedFunction: string " + classInstanceIdentifier + " " + Task.GetState(this) + " " + s);
            Task.SetSucceeded(this);
        }

	    [Task]
	    public void OverloadedFunction(SampleEnum e) {
		    if (debug)
			    Debug.Log("TestScript.PublicMultiFunction: enum " + classInstanceIdentifier + " " + Task.GetState(this) + " " + e);
            Task.SetSucceeded(this);
        }
    }
}