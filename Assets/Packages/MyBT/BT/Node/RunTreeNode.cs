﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyBT {
    public class RunTreeNode : Node {
        const int recursionLimit = 200;

        public string runTreeNodeName;
        public TreeNode runNode;

        public RunTreeNode(TaskController _taskController, int _nodeDepth, int[] _tokenIndexes) : base(_taskController, _nodeDepth, _tokenIndexes) {
        }

        public override NodeResult Tick(NodeExecute nodeExecute, int recursions = 0) { //, TaskRequestState requestState = TaskRequestState.nochange) {
            // base.PreTick(nodeExecute);

            // nodeResult = base.Tick(nodeExecute, recursions); 

            if (debugActive)
                Debug.Log(NodeLogger("EX", "", recursions));

            if (children.Count == 1) {
                if (recursions < recursionLimit) {
                    nodeResult = children[0].Tick(nodeExecute, recursions + 1);

                    // base.PostTick(nodeResult);
                    return nodeResult;
                }
                else {
                    Debug.LogError("TreeNode.Execute: too many recursions (>"+ recursionLimit + ") in " + runTreeNodeName);
                }
            }

            //if (recursions < 100) {
            //    if (runNode != null) {
            //		return runNode.HandleExecute(recursions + 1); //, requestState);
            //    }
            //    else {
            //		Debug.LogError("Execute." + this + " undefined runNode for " + runTreeNodeName);
            //    }
            //}

            // fail of execution
            Debug.LogError("Execute." + this + " too many recursions");
            nodeResult = NodeResult.Failed;
            base.PostTick(nodeResult);
            return nodeResult;
        }

        //public override void HandleStopping(int recursions, bool abort) {
        //    //base.HandleStopping(recursions, abort);
        //    if ((taskState == TaskState.stopped) || abort) {
        //        //taskState = TaskState.stopping;
        //        abort = true;
        //    }

        //    //runNode.HandleStopping(recursions + 1);
        //    if (children.Count == 1) {
        //        if (recursions < 100) {
        //            children[0].HandleStopping(recursions + 1, abort);
        //        }
        //        else {
        //            Debug.LogError("TreeNode.Execute: too many recursions in " + runTreeNodeName);
        //        }
        //    }

        //    //if (abort) {
        //    taskState = TaskState.firstrun;
        //    //}
        //}
    }

}