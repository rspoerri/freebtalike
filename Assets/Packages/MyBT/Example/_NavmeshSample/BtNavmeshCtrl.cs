﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using MyBT;

public class BtNavmeshCtrl : MonoBehaviour {

	Vector3 startPosition;
	Vector3 targetPosition;
	NavMeshAgent navMeshAgent;

	public void Start() {
		startPosition = transform.position;
		navMeshAgent = GetComponent<NavMeshAgent>();
	}

	[Task]
	public void Rotate() {
		if (Task.IsRunning(this)) {
			Debug.Log("Rotate");
			transform.Rotate(new Vector3(0, 30 * Time.deltaTime, 0));

			Task.SetSucceeded(this);
		}
	}

	[Task]
	public void RaycastCheck () {
		if (Task.IsRunning(this)) {
        	RaycastHit hit;

			Vector3 source = transform.position + Vector3.up * 1;
			// Does the ray intersect any objects excluding the player layer
			Debug.DrawRay(source, transform.TransformDirection(Vector3.forward) * 1000, Color.green);
			if (Physics.Raycast(source, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity)) {
				Debug.DrawRay(source, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
				//Debug.Log("Did Hit");
				if (hit.collider.CompareTag("Player")) {
					Debug.Log("Player Hit");
					targetPosition = hit.collider.transform.position;
					Task.SetSucceeded(this);
				}
			}
		}
	}

	[Task]
	public void MoveTo () {
		if (Task.IsStarting(this)) {
			
		}
		if (Task.IsRunning(this)) {
			navMeshAgent.SetDestination(targetPosition);
			if (ReachedDestination()) {
				Task.SetSucceeded(this);
			}
		}
	}

	[Task]
	public void Avoid () {
		if (Task.IsStarting(this)) {

		}
		if (Task.IsRunning(this)) {
			Vector3 realTargetPos = (transform.position - targetPosition) + transform.position;
			navMeshAgent.SetDestination(realTargetPos);
			if (ReachedDestination()) {
				Task.SetSucceeded(this);
			}
		}
	}

	[Task]
	public void Return () {
		if (Task.IsStarting(this)) {
			navMeshAgent.SetDestination(startPosition);
		}
		if (Task.IsRunning(this)) {
			if (ReachedDestination()) {
				Task.SetSucceeded(this);
			}
		}
	}

	[Task]
	public void SetVelocity (float speed) {
		if (Task.IsStarting(this)) {
			navMeshAgent.speed = speed;
		}
		Task.SetSucceeded(this);
	}


	public bool ReachedDestination() {
		if (!navMeshAgent.pathPending) {
			if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) {
				if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f) {
					return true;
				}
			}
		}
		return false;
	}

	public void SphereCast () {
		Vector3 source = transform.position + Vector3.up * 1;
		RaycastHit[] raycastHits = Physics.SphereCastAll(source, 5, Vector3.up);
		if (raycastHits.Length > 0) {
			foreach (RaycastHit rayHit in raycastHits) {
				if (rayHit.collider.gameObject.CompareTag("Player")) {
					Task.SetSucceeded(this);
					navMeshAgent.SetDestination(startPosition);
				}
			}
		}
	}

}
