﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

namespace MyBT
{
    [System.Serializable]
    public class TaskParameterGroup
    {
        List<TaskParameter> taskParameters;

        public TaskParameterGroup()
        {
            taskParameters = new List<TaskParameter>();
        }

        public TaskParameterGroup(MemberInfo memberInfo)
        {
            MethodInfo methodInfo = (MethodInfo)memberInfo;
            if (methodInfo != null)
            {
                taskParameters = new List<TaskParameter>();
                ParameterInfo[] parameterInfos = methodInfo.GetParameters();
                foreach (ParameterInfo parameterInfo in parameterInfos)
                {
                    TaskParameter taskParm = new TaskParameter() { valType = Type.GetTypeCode(parameterInfo.ParameterType) };
                    taskParameters.Add(taskParm);
                }
            }
            else
            {
                Debug.LogError("TaskParameter.TaskParameterGroup: methodInfo is null");
            }
        }

        public void Add(TaskParameter taskParameter)
        {
            taskParameters.Add(taskParameter);
        }

        public List<TaskParameter> GetAll()
        {
            return taskParameters;
        }

        public TaskParameter this[int index]
        {
            get
            {
                if ((0 <= index) && (index < taskParameters.Count))
                {
                    return taskParameters[index];
                }
                return null;
            }
        }

        public int Count
        {
            get
            {
                return taskParameters.Count;
            }
        }

        /// mostly for viewing in unity editor
        public string cachedString = "";
        public override string ToString()
        {
            List<string> taskParmStrs = new List<string>();
            foreach (TaskParameter taskParm in taskParameters)
            {
                taskParmStrs.Add(taskParm.valType.ToString());
            }
            cachedString = string.Join(",", taskParmStrs);
            //Debug.Log("TaskParameterGroup.ToString: " + cachedString + " (" + taskParameters.Count + ")");
            return cachedString;
        }


        public bool CheckCompatiblity(TaskParameterGroup taskParameterGroup)
        {
            // only allow same count of parameters
            if (this.Count != taskParameterGroup.Count)
                return false;
            //TaskParameterGroup parms = parameters;
            bool canConvert = true;
            // check wether the taskParameters can be converted to the parameters
            for (int i = 0; i < taskParameterGroup.Count; i++)
            {
                TaskParameter simpleValue = taskParameterGroup[i];
                TaskParameter complexValue = this[i];

                switch (simpleValue.valType)
                {
                    case TypeCode.DBNull:
                        // FAIL, invalid value
                        break;
                    case TypeCode.Boolean:
                        if ((new List<TypeCode> { TypeCode.Boolean, TypeCode.Int32, TypeCode.Single }).Contains(complexValue.valType))
                        {
                            // pass
                        }
                        else
                        {
                            canConvert = false;
                        }
                        break;
                    case TypeCode.Int32:
                        // can be integer, or an enum
                        if ((new List<TypeCode> { TypeCode.Int32, TypeCode.Single }).Contains(complexValue.valType))
                        {
                            // pass
                        }
                        else
                        {
                            canConvert = false;
                        }
                        break;
                    case TypeCode.Single:
                        if ((new List<TypeCode> { TypeCode.Single }).Contains(complexValue.valType))
                        {
                            // pass
                        }
                        else
                        {
                            canConvert = false;
                        }
                        break;
                    case TypeCode.Empty:

                        break;
                    case TypeCode.String:
                        if ((new List<TypeCode> { TypeCode.String }).Contains(complexValue.valType))
                        {
                            // pass
                        }
                        else
                        {
                            canConvert = false;
                        }
                        break;
                }

            }
            return canConvert;
        }
    }

    [System.Serializable]
    public class TaskParameter
    {
        // TypeCode.DBNull is considered invalid or undefined
        public System.TypeCode valType = TypeCode.DBNull;
        public object valValue = null;

        public TaskParameter()
        {
        }

        public TaskParameter(string stringValue)
        {
            bool boolValue;
            int intValue;
            float floatValue;

            Type thisType = Type.GetType(stringValue);
            if (thisType != null)
            {
                Debug.Log("TaskScriptParser.ParseValue: thisType " + thisType.FullName);
            }

            NumberStyles style = NumberStyles.Any;
            CultureInfo culture = CultureInfo.InvariantCulture;


            // Place checks higher in if-else statement to give higher priority to type.      
            if (bool.TryParse(stringValue, out boolValue))
            {
                valType = TypeCode.Boolean;
                valValue = boolValue;
                return;
            }
            else if (int.TryParse(stringValue, style, culture, out intValue))
            {
                valType = TypeCode.Int32;
                valValue = intValue;
                return;
            }
            else if (float.TryParse(stringValue, style, culture, out floatValue))
            {
                valType = TypeCode.Single;
                valValue = floatValue;
                return;
            }
            // handle string and enum
            else
            {
                // is null value
                //if (string.IsNullOrEmpty(stringValue)) {
                if (stringValue == "null")
                {
                    valType = TypeCode.Empty;
                    valValue = null;
                    return;
                }

                // is empty value
                if (stringValue == "")
                {
                    // no content
                    return;
                }

                string[] splitValue = stringValue.Split('"');

                // is string with ""
                if (splitValue.Length == 3)
                {
                    valType = TypeCode.String;
                    valValue = splitValue[1];
                    return;
                }

                // is enum?
                else
                {
                    string[] enumSplit = stringValue.Split('.');
                    try
                    {
                        System.Type enumType = GetEnumType(enumSplit[0]);
                        string convertValue = enumSplit[1];
                        object enumValue = Enum.Parse(enumType, convertValue);
                        valType = TypeCode.Int32;
                        valValue = enumValue;
                        return;
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("failed conversion of string '" + stringValue + "' error: " + e.ToString());
                        return;
                    }
                }
            }
        }

        public bool IsValid()
        {
            return (valType != TypeCode.DBNull);
        }

        public static Type GetEnumType(string enumName)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var type = assembly.GetType(enumName);
                if (type == null)
                    continue;
                if (type.IsEnum)
                    return type;
            }
            return null;
        }
    }
}