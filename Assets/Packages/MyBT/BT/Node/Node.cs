﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace MyBT {
    //public class CondictionNode : TaskNode {
    //	Task conditionTask;
    //	Task trueTask;
    //	Task falseTask;
    //}

    // used in HandleExecute
    public enum NodeExecute {
        // start or restart a node
        Run,
        // abort a running node
        Abort
    }
    public enum NodeState {
        // the node is not initialized
        NotRunning,
        // the Node has not been called before
        FirstRun,
        // state after FirstRun
        Running,
        // this node has to stop in this exec, also all children need to stop
        Aborting
    }
    public enum NodeResult {
        Running,
        Succeeded,
        Failed,
        Error
    }

    public class Node {
#if UNITY_EDITOR
        // used in editor inspector
        public bool isFolded = false;
#endif

        // only for testing
        public float floatValue1;
        public Material material;
        public int id;
        public bool boolValue;
        // testing end

        public Node parentNode;
        public List<Node> children = new List<Node>();
        public TaskController taskController;
        public int[] tokenIndexes;
        public int nodeDepth;
        public NodeResult nodeResult;

        public bool logActive = false;
        public string logString = "";

        public NodeState nodeState = NodeState.NotRunning;

        protected bool debugActive {
            get {
                // override log of taskController when local logging active
                if (logActive)
                    return logActive;
                return taskController.nodeConsoleDebugActive;
            }
        }

        public bool hasChildren {
            get {
                return ((children != null) && (children.Count > 0));
            }
        }

        public enum TaskNodeType {
            TaskNode,
            TreeNode,
            CompositeNode,
            DecoratorNode,
            ActionNode,
            RunTreeNode,
            CommentNode
        }

        #region node and task labels, icons, description and colors
        public static readonly Dictionary<Type, TaskNodeType> NodeTypeDict = new Dictionary<Type, TaskNodeType> {
            {typeof(Node), TaskNodeType.TaskNode},
            {typeof(TreeNode),TaskNodeType.TaskNode},
            {typeof(CompositeNode), TaskNodeType.CompositeNode},
            {typeof(DecoratorNode), TaskNodeType.DecoratorNode},
            {typeof(ActionNode), TaskNodeType.ActionNode},
            {typeof(RunTreeNode), TaskNodeType.RunTreeNode},
            {typeof(CommentNode), TaskNodeType.CommentNode},
        };

        public static readonly Dictionary<NodeState, string> NodeStateLabels = new Dictionary<NodeState, string>() {
            { NodeState.FirstRun,   "FIRR" },
            { NodeState.Running,    "RUNN" },
            { NodeState.Aborting,   "ABRT" },
            { NodeState.NotRunning, "NOTR" },
        };

        public static readonly Dictionary<NodeState, string> NodeStateDescriptions = new Dictionary<NodeState, string>() {
            { NodeState.FirstRun,   "FirstRun" },
            { NodeState.Running,    "Running" },
            { NodeState.Aborting,   "Aborting" },
            { NodeState.NotRunning, "NotRunning" },
        };

        public static readonly Dictionary<NodeState, Color> NodeStateColors = new Dictionary<NodeState, Color>() {
            { NodeState.FirstRun,   Color.yellow },
            { NodeState.Running,    Color.green },
            { NodeState.Aborting,   Color.red },
            { NodeState.NotRunning, Color.gray },
        };

        public static readonly Dictionary<NodeResult, string> NodeResultLabels = new Dictionary<NodeResult, string>() {
            { NodeResult.Running,   "rr"},
            { NodeResult.Succeeded, "tt"},
            { NodeResult.Failed,    "ff"},
            { NodeResult.Error,     "ee" },
        };

        public static readonly Dictionary<NodeResult, string> NodeResultDescriptions = new Dictionary<NodeResult, string>() {
            { NodeResult.Running,   "Running"},
            { NodeResult.Succeeded, "Succeeded"},
            { NodeResult.Failed,    "Failed"},
            { NodeResult.Error,     "Error"},
        };

        public static readonly Dictionary<NodeResult, Color> NodeResultColors = new Dictionary<NodeResult, Color>() {
            { NodeResult.Running, Color.yellow },
            { NodeResult.Succeeded,  Color.green },
            { NodeResult.Failed, Color.red },
            { NodeResult.Error,  Color.magenta},
        };

        public static readonly Dictionary<TaskState, string> TaskStateLabels = new Dictionary<TaskState, string>() {
            { TaskState.FirstRun,   "FIRR" },
            { TaskState.Running,    "RUNN" },
            { TaskState.Aborting,   "ABRT" },
            { TaskState.NotRunning, "NOTR" },
        };

        public static readonly Dictionary<TaskState, string> TaskStateDescriptions = new Dictionary<TaskState, string>() {
            { TaskState.FirstRun,   "FirstRun" },
            { TaskState.Running,    "Running" },
            { TaskState.Aborting,   "Aborting" },
            { TaskState.NotRunning, "NotRunning" },
        };

        public static readonly Dictionary<TaskState, Color> TaskStateColors = new Dictionary<TaskState, Color>() {
            { TaskState.FirstRun,   Color.yellow },
            { TaskState.Running,    Color.green },
            { TaskState.Aborting,   Color.red },
            { TaskState.NotRunning, Color.gray },
        };

        public static readonly Dictionary<TaskResult, string> TaskResultLabels = new Dictionary<TaskResult, string>() {
            { TaskResult.Running,   "rr"},
            { TaskResult.Succeeded, "tt"},
            { TaskResult.Failed,    "ff"},
            { TaskResult.Error,     "ee" },
        };

        public static readonly Dictionary<TaskResult, string> TaskResultDescriptions = new Dictionary<TaskResult, string>() {
            { TaskResult.Running,   "Running"},
            { TaskResult.Succeeded, "Succeeded"},
            { TaskResult.Failed,    "Failed"},
            { TaskResult.Error,     "Error"},
        };

        public static readonly Dictionary<TaskResult, Color> TaskResultColors = new Dictionary<TaskResult, Color>() {
            { TaskResult.Running, Color.yellow },
            { TaskResult.Succeeded,  Color.green },
            { TaskResult.Failed, Color.red },
            { TaskResult.Error,  Color.magenta},
        };

#if UNITY_EDITOR
        public static readonly Dictionary<TaskState, Texture2D> TaskStateIcons = new Dictionary<TaskState, Texture2D>() {
            { TaskState.NotRunning, EditorGUIUtility.Load("MyBT/Sleep16.png") as Texture2D },
            { TaskState.FirstRun,   EditorGUIUtility.Load("MyBT/One16.png") as Texture2D },
            { TaskState.Running,    EditorGUIUtility.Load("MyBT/Repeat16.png") as Texture2D },
            { TaskState.Aborting,   EditorGUIUtility.Load("MyBT/Alert16.png") as Texture2D },
        };

        public static readonly Dictionary<NodeState, Texture2D> NodeStateIcons = new Dictionary<NodeState, Texture2D>() {
            { NodeState.NotRunning, EditorGUIUtility.Load("MyBT/Sleep16.png") as Texture2D },
            { NodeState.FirstRun,   EditorGUIUtility.Load("MyBT/One16.png") as Texture2D },
            { NodeState.Running,    EditorGUIUtility.Load("MyBT/Repeat16.png") as Texture2D },
            { NodeState.Aborting,   EditorGUIUtility.Load("MyBT/Alert16.png") as Texture2D },
        };

        public static readonly Dictionary<TaskResult, Texture2D> TaskResultIcons = new Dictionary<TaskResult, Texture2D>() {
            { TaskResult.Running,   EditorGUIUtility.Load("MyBT/Play16.png") as Texture2D },
            { TaskResult.Succeeded, EditorGUIUtility.Load("MyBT/True16.png") as Texture2D },
            { TaskResult.Failed,    EditorGUIUtility.Load("MyBT/False16.png") as Texture2D },
            { TaskResult.Error,     EditorGUIUtility.Load("MyBT/Bug16.png") as Texture2D },
        };

        public static readonly Dictionary<NodeResult, Texture2D> NodeResultIcons = new Dictionary<NodeResult, Texture2D>() {
            { NodeResult.Running,   EditorGUIUtility.Load("MyBT/Play16.png") as Texture2D },
            { NodeResult.Succeeded, EditorGUIUtility.Load("MyBT/True16.png") as Texture2D },
            { NodeResult.Failed,    EditorGUIUtility.Load("MyBT/False16.png") as Texture2D },
            { NodeResult.Error,     EditorGUIUtility.Load("MyBT/Bug16.png") as Texture2D },
        };
#endif
        #endregion

        public Node(TaskController _taskController, int _nodeDepth, int[] _tokenIndexes) {
            taskController = _taskController;
            tokenIndexes = _tokenIndexes;
            nodeDepth = _nodeDepth;
        }

        public virtual Node Add(Node n) {
            if (children == null) {
                children = new List<Node>();
            }

            // add new child to this
            children.Add(n);

            // set parent in child
            n.parentNode = this;

            return n;
        }

        //public static IEnumerator<Node> GetEnumerator(Node node) {
        //	yield return node;
        //	foreach (Node child in node.children) {
        //		//yield return child;
        //		if (child.children.Count > 0) {
        //			foreach (Node subChild in child) {
        //				yield return subChild;
        //			}
        //		}
        //	}
        //}

        /// <summary>
        /// reset the whole behaviour tree structure
        /// </summary>
        public virtual void Reset() {
            if (debugActive) {
                Debug.Log(NodeLogger("Reset", "", 0));
            }
            taskController = null;
            tokenIndexes = null;
            nodeDepth = 0;
            parentNode = null;
            children = new List<Node>();
            nodeResult = NodeResult.Running;
            logActive = false;
            logString = "";
        }

        public IEnumerable<Node> getAllNodesRecursively(bool traverseRunTree) { //Node subnode) { 
                                                                                // Return the parent before its children
            yield return this;

            if (!(this is RunTreeNode) || traverseRunTree) {
                foreach (Node node in this.children) {
                    foreach (Node n in node.getAllNodesRecursively(traverseRunTree)) {
                        yield return n;
                    }
                }
            }
        }

        public void PreTick (NodeExecute nodeExecute) {
            switch (nodeExecute) {
                case NodeExecute.Run:
                    switch (nodeState) {
                        case NodeState.NotRunning:
                            nodeState = NodeState.FirstRun;
                            break;
                        case NodeState.FirstRun:
                            nodeState = NodeState.Running;
                            break;
                        case NodeState.Running:
                            nodeState = NodeState.Running;
                            break;
                        case NodeState.Aborting:
                            nodeState = NodeState.FirstRun;
                            break;
                    }
                    break;

                case NodeExecute.Abort:
                    switch (nodeState) {
                        case NodeState.NotRunning:
                            nodeState = NodeState.NotRunning;
                            break;
                        case NodeState.FirstRun:
                            nodeState = NodeState.Aborting;
                            break;
                        case NodeState.Running:
                            nodeState = NodeState.Aborting;
                            break;
                        case NodeState.Aborting:
                            nodeState = NodeState.Aborting;
                            break;
                    }
                    break;
            }
        }

        public virtual NodeResult Tick(NodeExecute nodeExecute, int recursions) {
            return NodeResult.Running;
        }

        public virtual void PostTick(NodeResult nodeResult) {
            switch (nodeResult) {
                case NodeResult.Running:
                    nodeState = NodeState.Running;
                    break;
                case NodeResult.Succeeded:
                    nodeState = NodeState.NotRunning;
                    break;
                case NodeResult.Failed:
                    nodeState = NodeState.NotRunning;
                    break;
                case NodeResult.Error:
                    nodeState = NodeState.Aborting;
                    break;
           }
        }


        public void DetachChildren() {
            if (children != null) {
                foreach (Node child in children) {
                    child.DetachChildren();
                    child.parentNode = null;
                }
                children = null;
            }
        }

        public string NodeLogger(string functionName, string appendix, int recursions = 0) {
            string stateLabel = " nodeState " + NodeStateLabels[nodeState];
            if (this is ActionNode) {
                ActionNode an = (ActionNode)this;
                stateLabel += " taskState " + TaskStateLabels[an.taskState];
            }
            string output = RecursionIndention(recursions)
                + " " + stateLabel
                + " " + functionName
                + ": " + codeLine
                + " (" + lineNumber + ":" + colNumber + ") "
                + appendix + " (frame="+Time.frameCount+")";
            return output;
        }

        public string RecursionIndention(int recursion) {
            return new String('-', 1 + recursion * 2);
        }

        public int lineNumber {
            get {
                if ((tokenIndexes != null) && (tokenIndexes.Length > 0)) {
                    return taskController.tokens[tokenIndexes[0]].line;
                }
                return 0;
            }
        }
        public int colNumber {
            get {
                if ((tokenIndexes != null) && (tokenIndexes.Length > 0)) {
                    return taskController.tokens[tokenIndexes[0]].col;
                }
                return 0;
            }
        }

        public virtual string text {
            get {
                return codeLine;
            }
        }

        public string codeLine {
            get {
                string tokenString = "";
                if (tokenIndexes != null) {
                    for (int i = tokenIndexes[0]; i < tokenIndexes[1]; i++) {
                        tokenString += taskController.tokens[i].GetValue();
                    }
                }
                return tokenString;
            }
        }

        public string spacedText {
            get {
                string tokenString = "";
                if (tokenIndexes != null) {
                    for (int i = tokenIndexes[0]; i < tokenIndexes[1]; i++) {
                        tokenString += " " + Regex.Replace(taskController.tokens[i].GetValue(), @"\t|\n|\r", " ");
                    }
                }
                return tokenString;
            }
        }


        public string DebugTree(int curDepth = 0) {
            string output = new string('-', curDepth) + ToString() + " " + text + "\n";
            if (children != null) {
                foreach (Node taskNode in children) {
                    output += taskNode.DebugTree(curDepth + 1);
                }
            }
            return output;
        }
    }
}