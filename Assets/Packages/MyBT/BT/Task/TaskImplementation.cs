﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace MyBT
{
    [System.Serializable]
    public class TaskImplementation
    {
        public MemberInfo memberInfo;

        public object implementation;
        //public List<TaskParameter> parameterTypesNew;
        public TaskParameterGroup parameters;

        //public System.Action systemAction;
        //public Task task;

        public string fullName;
        public string namespaceName;
        public string className;
        public string memberName;

        public TaskImplementation(MemberInfo _memberInfo, Component component)
        {
            // https://stackoverflow.com/questions/607178/how-enumerate-all-classes-with-custom-class-attribute
            memberInfo = _memberInfo;

            System.Type classType = memberInfo.DeclaringType;
            string namespaceClassMemberName = classType.FullName + "." + memberInfo.Name;

            switch (memberInfo.MemberType)
            {
                case MemberTypes.Method:
                    //MethodInfo methodInfo = (MethodInfo)memberInfo;

                    fullName = namespaceClassMemberName;
                    namespaceName = classType.Namespace;
                    className = classType.Name;
                    memberName = memberInfo.Name;

                    parameters = new TaskParameterGroup(memberInfo);
                    implementation = component;

                    break;
                default:
                    //Debug.LogWarning("TaskImplementation.TaskImplementation: Unknown MemberType " + memberInfo.MemberType);
                    break;
            }
        }

        //public bool HasTask() {
        //	return (task != null);
        //}

        public bool HasImplementation()
        {
            return (implementation != null);
        }

        public override string ToString()
        {
            return "name='" + this.fullName + "' : implementation " + (this.implementation != null); // + ", task " + (this.task != null);
        }

        public void GetAction(TaskParameterGroup taskParameterGroup, out Action returnAction)
        {
            returnAction = null;

            List<TaskParameter> executionTaskParameters = taskParameterGroup.GetAll();
            if (executionTaskParameters.Count != parameters.Count)
            {
                Debug.LogWarning("TaskImplementation.ExecuteTask: invalid parameter count: " + this.fullName + " " + executionTaskParameters.Count + " " + parameters.Count);
                return;
            }

            // verify and convert parameters
            List<object> parametersList = new List<object>();
            for (int i = 0; i < parameters.Count; i++)
            {
                if (executionTaskParameters[i].valType == parameters[i].valType)
                {
                    try
                    {
                        parametersList.Add(executionTaskParameters[i].valValue);
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TaskImplementation.ExecuteTask: cannot convert parameter: " + i + " " + executionTaskParameters[i] + " : " + e.ToString());
                        return;
                    }
                }
                else
                {
                    //Debug.LogWarning("TaskImplementation.ExecuteTask: trying conversion of parameter: " + i + " " + executionTaskParameters[i] + " " + executionTaskParameters[i].valType + " " + parameterTypesNew[i].valType);            
                    object convertedValue = Convert.ChangeType(executionTaskParameters[i].valValue, parameters[i].valType);
                    parametersList.Add(convertedValue);
                }
            }
            object[] executionParameters = parametersList.ToArray();

            // call a method
            MethodInfo methodInfo = (MethodInfo)memberInfo;
            if (methodInfo.ReturnType == typeof(void))
            {
                if (this.parameters.Count > 0)
                {
                    returnAction = () => methodInfo.Invoke(implementation, executionParameters);
                }
                else
                {
                    returnAction = System.Delegate.CreateDelegate(typeof(System.Action), implementation, methodInfo) as System.Action;
                }
            }
            else
            {
                // for the moment dont handle methods with return value differently then the ones without return value
                if (this.parameters.Count > 0)
                {
                    returnAction = () => methodInfo.Invoke(implementation, executionParameters);
                }
                else
                {
                    returnAction = System.Delegate.CreateDelegate(typeof(System.Action), implementation, methodInfo) as System.Action;
                }
            }

            /*
            // Maybe do this a later time, assigning values, 
            // if it's a field
            FieldInfo fieldInfo = memberInfo as FieldInfo;
            if (fieldInfo != null) {
                Debug.Log("TaskImplementation.ExecuteTask: FieldInfo");
                returnAction = () => fieldInfo.GetValue(implementation);
            }

            // if it's a property
            PropertyInfo propertyInfo = memberInfo as PropertyInfo;
            if (propertyInfo != null) {
                Debug.Log("TaskImplementation.ExecuteTask: PropertyInfo");
                returnAction = () => propertyInfo.GetValue(implementation, null);
            }
            */
        }
    }
}