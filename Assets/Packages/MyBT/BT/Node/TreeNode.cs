﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MyBT {
    public class TreeNode : Node {
        public string name;

        public TreeNode(TaskController _taskController, int _nodeDepth, int[] _tokenIndexes, string _name) 
            : base(_taskController, _nodeDepth, _tokenIndexes) {
            name = _name;
        }

        public override NodeResult Tick(NodeExecute nodeExecute, int recursions) {
            if (debugActive)
                Debug.Log(NodeLogger("Execute", "", recursions));

            base.PreTick(nodeExecute);

            //base.Tick(nodeExecute, recursions);
            // switch (nodeExecute) {
            //     case NodeExecute.Run:
            //         switch (nodeState) {
            //             case NodeState.NotRunning:
            //                 nodeState = NodeState.FirstRun;
            //                 break;
            //             case NodeState.FirstRun:
            //                 nodeState = NodeState.Running;
            //                 break;
            //             case NodeState.Running:
            //                 //nodeState = NodeState.Running;
            //                 break;
            //             case NodeState.Aborting:
            //                 nodeState = NodeState.FirstRun;
            //                 break;
            //         }
            //         break;

            //     case NodeExecute.Abort:
            //         switch (nodeState) {
            //             case NodeState.NotRunning:
            //                 nodeState = NodeState.NotRunning;
            //                 break;
            //             case NodeState.FirstRun:
            //                 nodeState = NodeState.Aborting;
            //                 break;
            //             case NodeState.Running:
            //                 nodeState = NodeState.Aborting;
            //                 break;
            //             case NodeState.Aborting:
            //                 // can happen
            //                 break;
            //         }
            //         break;
            // }

            // this should ignore comment nodes, but it currently does not!!!
            if (children.Count == 1) {
                if (recursions < 100) {
                    nodeResult = children[0].Tick(nodeExecute, recursions + 1);
                    base.PostTick(nodeResult);
                    return nodeResult;
                }
                else {
                    Debug.LogError("TreeNode.Execute: too many recursions in " + name);
                }
            }

            else { 
                // fail of execution
                Debug.LogError("TreeNode.Execute: invalid child count " + children.Count + " in " + name + " CAN BE CAUSED BY COMMENT!!!");
                nodeResult = NodeResult.Error;
                base.PostTick(nodeResult);
            }
            return nodeResult;
        }
    }
}