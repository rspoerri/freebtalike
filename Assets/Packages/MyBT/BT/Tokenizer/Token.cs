﻿using System.Text.RegularExpressions;
namespace MyBT
{
    public class Token {
        TaskController controller;
        public int start { get; private set; }
        public int length { get; private set; }
        public int line, col;
        public SyntaxLabels type { get; private set; }

        public Token(TaskController _env, SyntaxLabels _type, int _start, int _length, int _line, int _col) {
            this.controller = _env;
            this.type = _type;
            this.start = _start;
            this.length = _length;
            this.line = _line;
            this.col = _col;
            string code = controller.code.Substring(start, length);
        }

        public void Reset() {
            this.controller = null;
            this.type = SyntaxLabels.ActionNameKeyword;
            this.start = 0;
            this.length = 0;
            this.line = 0;
            this.col = 0;
        }

        public string GetLocation() => $"<line:{line},col:{col}>";

        public override string ToString() => $"<{type}> start: {start} length: {length} value: {evaluate()}";
        public string evaluate() => $"'{Regex.Escape(controller.code.Substring(start, length))}'";
        public string GetValue() => controller.code.Substring(start, length);
    }
}