﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MyBT;

public class StructureTest : MonoBehaviour {

	[Task]
	public void SucceedImmediate (string s) {
		Debug.Log("SucceedImmediate " + s + " " + Task.GetState(this));
		if (Task.IsRunning(this)) {
			//if (Task.Debug(this))
				//Debug.Log("SucceedImmediate " + s + " " + Task.GetState(this));
			Task.SetSucceeded(this);
		}
	}

	[Task]
	public void FailImmediate (string s) {
		Debug.Log("FailImmediate " + s + " " + Task.GetState(this));
		if (Task.IsRunning(this)) {
			//if (Task.Debug(this))
				//Debug.Log("FailImmediate "+ s);
			Task.SetFailed(this);
		}
	}
}
