﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBT;

public class Test : MonoBehaviour
{
    [Task]
    void Succeed () {
        Debug.Log("Test.Succeed: " + Task.getState);
        if (Task.isStarting) {
            Task.SetSucceed();
        }
    }

    [Task]
    void Fail () {
        Debug.Log("Test.Fail: " + Task.getState);
        if (Task.isStarting) {
            Task.SetSucceed();
        }
    }

    [Task]
    void Wait (float duration) {
        Debug.Log("Test.Wait: " + Task.getState);
        if (Task.isStarting) {
            Task.data = Time.time;
        } else {
            float startTime = (float)Task.data;
            float timePassed = (Time.time - startTime);
            Task.log = timePassed.ToString();
            if (timePassed > duration)
                Task.SetSucceed();
        }
    }
}
