﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace MyBT {
    public class NodeGenerator {
        // current index we are handling in the token list, used while generating
        int tokenIndex = 0;
        // the current parent node under which we are generating nodes
        Node currentParentNode = null;
        int currentNodeDepth = 0;

        // we set the treeNodes on the taskController
        TaskController taskController;

        bool debugActive = false;

        public List<Token> tokens {
            get {
                return taskController.tokens;
            }
        }

        public List<Node> treeNodes {
            get {
                return taskController.treeRootNodes;
            }
        }

        public NodeGenerator(TaskController _taskController) {
            taskController = _taskController;
        }

        public bool Generate() {
            tokenIndex = 0;
            currentParentNode = null;
            currentNodeDepth = 0;

            // log output the tokens
            if (debugActive) {
                foreach (Token t in tokens) {
                    Debug.Log(t);
                }
            }

            if (debugActive)
                Debug.Log("------------ START NODE GENERATION ----------------");
            // generate the node tree from the token list
            int failsaveCounter = tokens.Count;
            while (tokenIndex < tokens.Count) {
                try { 
                    ParseTokens();
                } catch (Exception ex) {
                    Debug.LogError("ParseTokens Raised Error "+ex+" "+ex.Message+" "+ex.InnerException+" "+ex.StackTrace);
                    return false;
                }
                failsaveCounter--;
                if (failsaveCounter <= 0) {
                    Debug.LogError("Failsave used to abort token parsing");
                    return false;
                }
            }
            return true;
        }

        public void ParseTokens() {
            // switch on next token in script
            switch (tokens[tokenIndex].type) {
                case SyntaxLabels.TreeNameKeyword:
                    if (currentParentNode == null) {
                        ReadTreeNode();
                    }
                    else {
                        Debug.Log("unexpected keyword outside of TreeNode " + tokens[tokenIndex].type + " @ " + tokens[tokenIndex]);
                        //Debug.LogError("Invalid SyntaxKind Encountered outside of TreeNode " + tokens[tokenIndex].type + " " + tokens[tokenIndex]);
                    }
                    break;
                case SyntaxLabels.RunTreeNameKeyword:
                    ReadRunTreeNode();
                    break;
                case SyntaxLabels.Function:
                    ReadFunctionNode();
                    break;
                case SyntaxLabels.CompositeNameKeyword:
                    if (Node.NodeTypeDict[currentParentNode.GetType()] == Node.TaskNodeType.TreeNode) {
                        // valid
                    }
                    ReadCompositeNode();
                    break;
                case SyntaxLabels.CloseCurlyBracket:
                    PopNode();
                    break;
                case SyntaxLabels.EOL:
                case SyntaxLabels.Whitespace:
                    // just ignore whitespace and EOL
                    tokenIndex += 1;
                    break;
                case SyntaxLabels.SingleLineCommentStart:
                    ReadSingleLineCommentNode();
                    //tokenIndex += 1;
                    break;
                case SyntaxLabels.MultiLineCommentStart:
                    ReadMultiLineCommentNode();
                    //tokenIndex += 1;
                    break;
                case SyntaxLabels.MultiLineCommentEnd:
                    //ReadMultiLineCommentNode();
                    tokenIndex += 1;
                    break;
                //case SyntaxKind.ActionNameToken:
                //  if (debugActive) 
                //      Debug.LogError("Invalid SyntaxKind Encountered " + tokens[tokenIndex].type + " " + tokens[tokenIndex]);
                //  tokenIndex += 1;
                //  break;
                //case SyntaxKind.DecoratorNameToken:
                //  if (debugActive) 
                //      Debug.LogError("Invalid SyntaxKind Encountered " + tokens[tokenIndex].type + " " + tokens[tokenIndex]);
                //  tokenIndex += 1;
                //  break;
                default:
                    Debug.LogWarning("Unhandled SyntaxKind Encountered " + tokens[tokenIndex].type + " " + tokens[tokenIndex]);
                    tokenIndex += 1;
                    break;
            }
        }


        public void ReadTreeNode() {
            Require(SyntaxLabels.TreeNameKeyword);
            int firstTokenIndex = tokenIndex;
            Require(SyntaxLabels.OpenNormalBracket);
            string tokenName = ReadLiteralString();
            Require(SyntaxLabels.CloseNormalBracket);
            int lastTokenIndex = tokenIndex;
            Require(SyntaxLabels.OpenCurlyBracket);

            // we started a new tree node with opening
            currentParentNode = new TreeNode(taskController, currentNodeDepth, new int[] { firstTokenIndex-1, lastTokenIndex }, tokenName);
            currentNodeDepth += 1;

            if (debugActive)
                Debug.Log("ReadTreeNode " + tokenName);

            // add to tree list
            treeNodes.Add(currentParentNode);
        }

        public void ReadCompositeNode() {
            int firstTokenIndex = tokenIndex;
            // under a tree node we except a composite node
            Require(SyntaxLabels.CompositeNameKeyword);
            Require(SyntaxLabels.OpenNormalBracket);
            //string compositeType = ReadLiteralString();
            List<string> compositeParametersList = ReadStringVariableList();
            Require(SyntaxLabels.CloseNormalBracket);
            int lastTokenIndex = tokenIndex;
            Require(SyntaxLabels.OpenCurlyBracket);

            // we started a composite node
            CompositeNode compositeNode = new CompositeNode(taskController, currentNodeDepth, new int[] { firstTokenIndex, lastTokenIndex }, compositeParametersList);
            currentParentNode.Add(compositeNode);
            currentNodeDepth += 1;

            // set it as new parent node
            currentParentNode = compositeNode;
            if (debugActive)
                Debug.Log("ReadCompositeNode");
        }

        public void ReadFunctionNode() {
            int firstTokenIndex = tokenIndex;
            string functionName = "";
            if (tokens[tokenIndex].type == SyntaxLabels.Function) {
                functionName = tokens[tokenIndex].GetValue();
                tokenIndex += 1;
            }
            //Require(SyntaxKind.FunctionNameToken);
            Require(SyntaxLabels.OpenNormalBracket);
            //List<object> compositeParametersList = ReadLiteralTokenList();
            TaskParameterGroup taskParameterGroup = ReadLiteralTokenListAsTaskParameterGroup();
            Require(SyntaxLabels.CloseNormalBracket);
            int lastTokenIndex = tokenIndex;

            Require(SyntaxLabels.EOL);

            ActionNode actionNode = new ActionNode(taskController, currentNodeDepth, new int[] { firstTokenIndex, lastTokenIndex }, functionName, taskParameterGroup); // { functionName = functionName, paramters = compositeParametersList };
            currentParentNode.Add(actionNode);
            //currentNodeDepth += 1;

            if (debugActive)
                Debug.Log("ReadFunctionNode");
        }


        public void ReadSingleLineCommentNode() {
            int firstTokenIndex = tokenIndex;
            tokenIndex += 1;
            string comment = "// " + tokens[tokenIndex].GetValue();
            CommentNode commentNode = new CommentNode(taskController, currentNodeDepth, new int[] { firstTokenIndex, tokenIndex }, comment);
            if (currentParentNode != null) {
                currentParentNode.Add(commentNode);
            }
            else {
                Debug.LogWarning("cannot currently add comment outside of Tree");
            }
        }

        public void ReadMultiLineCommentNode () {
            int firstTokenIndex = tokenIndex;
            tokenIndex += 1;
            string comment = "// " + tokens[tokenIndex].GetValue();
            CommentNode commentNode = new CommentNode(taskController, currentNodeDepth, new int[] { firstTokenIndex, tokenIndex }, comment);
            if (currentParentNode != null) {
                currentParentNode.Add(commentNode);
            }
            else {
                Debug.LogWarning("cannot currently add comment outside of Tree");
            }
        }

        public void ReadRunTreeNode() {
            int firstTokenIndex = tokenIndex;
            //string treeName = "";
            if (tokens[tokenIndex].type == SyntaxLabels.RunTreeNameKeyword) {
                //treeName = tokens[tokenIndex].GetValue();
                tokenIndex += 1;
            }
            Require(SyntaxLabels.OpenNormalBracket);
            string runTreeName = ReadLiteralString();
            Require(SyntaxLabels.CloseNormalBracket);
            int lastTokenIndex = tokenIndex;
            Require(SyntaxLabels.EOL);

            RunTreeNode runTreeNode = new RunTreeNode(taskController, currentNodeDepth, new int[] { firstTokenIndex, lastTokenIndex }) { runTreeNodeName = runTreeName };
            currentParentNode.Add(runTreeNode);
            //currentNodeDepth += 1;
        }

        public void PopNode() {
            // set the parent of the current parent as new parent
            // we go up one element in the structure, because of a closing bracket
            if (currentParentNode != null) {
                if (debugActive)
                    Debug.Log("PopNode: cur '" + currentParentNode + "' par '" + currentParentNode.parentNode + "' " + tokens[tokenIndex]);
                if (currentParentNode.parentNode == null) {
                    if (debugActive)
                        Debug.Log("PopNode: Finished tree " + currentParentNode.DebugTree());
                }
                currentNodeDepth -= 1;
                currentParentNode = currentParentNode.parentNode;
                tokenIndex += 1;
            }
            // no parent
            else {
                Debug.LogError("PopNode: no parent node");
            }
        }

        public string ReadLiteralString() {
            if (tokens[tokenIndex].type == SyntaxLabels.StringSymbol) {
                string value = tokens[tokenIndex].GetValue().Trim('"');
                //Debug.Log("NodeGenerator.ReadLiteralString: read string '" + value + "'");
                tokenIndex = tokenIndex + 1;
                return value;
            }
            else {
                Debug.LogError("NodeGenerator.ReadLiteralString: wrong token " + tokens[tokenIndex].type + " @ " + tokens[tokenIndex]);
                StopExecution();
                return "";
            }
        }

        public List<string> ReadStringVariableList() {
            List<string> variableList = new List<string>();
            while (tokens[tokenIndex].type == SyntaxLabels.Variable) {
                variableList.Add(tokens[tokenIndex].GetValue());
                tokenIndex += 1;
                if (tokens[tokenIndex].type == SyntaxLabels.Comma) {
                    tokenIndex += 1;
                }
            }
            return variableList;
        }

        //public List<object> ReadLiteralTokenList() {
        //    List<object> variableList = new List<object>();
        //    while ((tokens[tokenIndex].type == SyntaxKind.LiteralStringToken)
        //           || (tokens[tokenIndex].type == SyntaxKind.LiteralNumberToken)
        //           || (tokens[tokenIndex].type == SyntaxKind.LiteralCharToken)
        //           || (tokens[tokenIndex].type == SyntaxKind.VariableNameToken)
        //          ) {
        //        variableList.Add(tokens[tokenIndex].GetValue());
        //        tokenIndex += 1;
        //        if (tokens[tokenIndex].type == SyntaxKind.CommaToken) {
        //            tokenIndex += 1;
        //        }
        //    }
        //    return variableList;
        //}

        public TaskParameterGroup ReadLiteralTokenListAsTaskParameterGroup() {
            TaskParameterGroup taskParameterGroup = new TaskParameterGroup();

            while ((tokens[tokenIndex].type == SyntaxLabels.StringSymbol)
                   || (tokens[tokenIndex].type == SyntaxLabels.NumberSymbol)
                   || (tokens[tokenIndex].type == SyntaxLabels.CharSymbol)
                   || (tokens[tokenIndex].type == SyntaxLabels.Variable)
                   || (tokens[tokenIndex].type == SyntaxLabels.TrueKeyword)
                   || (tokens[tokenIndex].type == SyntaxLabels.FalseKeyword)
                  ) {
                TaskParameter taskParameter = new TaskParameter(tokens[tokenIndex].GetValue());
                taskParameterGroup.Add(taskParameter);
                //variableList.Add(tokens[tokenIndex].GetValue());
                tokenIndex += 1;
                if (tokens[tokenIndex].type == SyntaxLabels.Comma) {
                    tokenIndex += 1;
                }
                if (tokens[tokenIndex].type == SyntaxLabels.Whitespace) {
                    tokenIndex += 1;
                }
            }

            return taskParameterGroup;
        }

        public void Skip(SyntaxLabels kind) {
            if (tokens[tokenIndex].type == kind) {
                tokenIndex = tokenIndex + 1;
            }
        }

        public void SkipWhileSyntax(SyntaxLabels kind) {
            while ((tokenIndex < tokens.Count) && (tokens[tokenIndex].type == kind)) {
                tokenIndex += 1;
            }
        }

        static readonly ReadOnlyCollection<SyntaxLabels> requireCanIgnore = new ReadOnlyCollection<SyntaxLabels>(
            new[] {
                SyntaxLabels.Whitespace,
                SyntaxLabels.EOL,
                SyntaxLabels.SingleLineCommentStart
            } 
        );

        public void Require(SyntaxLabels kind) {
            while (tokenIndex < tokens.Count) {
                // return when token found
                if (tokens[tokenIndex].type == kind) {
                    if (debugActive)
                        Debug.Log("Require " + kind + " : found " + tokens[tokenIndex].type + " @ " + tokens[tokenIndex].line + ":" + tokens[tokenIndex].col);
                    tokenIndex = tokenIndex + 1;
                    return;
                }
                // skip SyntaxLabels in requireCanIgnore, othervise abort with error
                else {
                    if (requireCanIgnore.Contains(tokens[tokenIndex].type)) {
                        // valid to skip this type of token
                        if (debugActive)
                            Debug.Log("Require " + kind + " : skipping " + tokens[tokenIndex].type + " @ " + tokens[tokenIndex].line + ":" + tokens[tokenIndex].col);
                        tokenIndex = tokenIndex + 1;
                    }
                    else {
                        Debug.LogError("Require " + kind + " : skipping NOT ALLOWED " + tokens[tokenIndex].type + " " + tokens[tokenIndex].GetValue() + " @ " + tokens[tokenIndex].line + ":" + tokens[tokenIndex].col);
                        StopExecution();
                        return;
                    }
                }
            }

            Debug.LogError("Require: " + kind + " @ out of bounds " + tokenIndex + " after line " + tokens[tokenIndex - 1].line);
            StopExecution();
            return;
        }

        public void StopExecution() {
            tokenIndex = tokens.Count;
        }
    }
}