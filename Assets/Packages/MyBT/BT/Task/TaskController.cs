﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Runtime.Serialization.Configuration;

namespace MyBT {
#if UNITY_EDITOR
    using UnityEditor.IMGUI.Controls;
    using UnityEditor;
    public class DetectChanges : AssetPostprocessor {
        public static bool autodetectFilechanges = true;

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
            if (autodetectFilechanges) {
                List<string> changedFiles = new List<string>();
                foreach (string str in importedAssets) {
                    //Debug.Log("Reimported Asset: " + str);
                    string[] pathSplitStr = str.Split('/');
                    string[] fileSplitStr = pathSplitStr[pathSplitStr.Length - 1].Split('.');

                    //string folder = pathSplitStr[pathSplitStr.Length - 3];
                    string fileName = string.Join(".", fileSplitStr, 0, fileSplitStr.Length - 1); //fileSplitStr[fileSplitStr.Length - 2];
                                                                                                  //Debug.Log("File name: " + fileName);
                                                                                                  //string extension = fileSplitStr[fileSplitStr.Length - 1];
                                                                                                  //Debug.Log("File type: " + extension);
                    changedFiles.Add(fileName);
                }

                //TaskController[] taskControllers = Resources.FindObjectsOfTypeAll<TaskController>();
                foreach (TaskController tc in Resources.FindObjectsOfTypeAll<TaskController>()) {
                    // unmutable objects
                    if (tc.hideFlags == HideFlags.NotEditable || tc.hideFlags == HideFlags.HideAndDontSave) {
                        Debug.Log("NotEditable || HideAndDontSave");
                        continue;
                    }

                    // is a file on disk
                    //if (!EditorUtility.IsPersistent(tc.transform.root.gameObject)) {
                    //	Debug.Log("IsPersistent");
                    //	continue;
                    //}

                    // everything else is a scene TaskController
                    //Debug.Log("TaskController Script Name: " + tc.taskScriptZwo.name);
                    if (changedFiles.Contains(tc.taskScriptZwo.name)) {
                        Debug.Log("Detected script " + tc.taskScriptZwo.name + " has changed");
                        tc.ResetData();
                    }
                }

                //foreach (string str in deletedAssets) {
                //	Debug.Log("Deleted Asset: " + str);
                //}

                //for (int i = 0; i < movedAssets.Length; i++) {
                //	Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);
                //}
            }
        }
    }

    internal class TreeViewItem<T> : TreeViewItem where T : Node {
        public T data { get; set; }

        public TreeViewItem(int id, int depth, string displayName, T data) : base(id, depth, displayName) {
            this.data = data;
        }
        public TreeViewItem(int id, int depth, T data) : base(id, depth) {
            this.data = data;
        }
        public TreeViewItem(int id, T data) : base(id) {
            this.data = data;
        }
    }

    class TaskTreeView : TreeView {

        TaskController taskController;
        int index;

        float kRowHeights = 20;
        float kToggleWidth = 40f;
        public bool showControls = true;

        // sorts the columns
        public enum ColumnSorting {
            LineNumber,
            Code,
            TaskState,
            ResultCache,
            Log,
            LogResult,
        }

        public TaskTreeView(TreeViewState state, MultiColumnHeader _multiColumnHeader) : base(state, _multiColumnHeader) {
            // Custom setup
            Init(_multiColumnHeader);
            Reload();
            ExpandAll();
        }

        public TaskTreeView(TreeViewState treeViewState, MultiColumnHeader _multiColumnHeader, TaskController _taskController, int _index) : base(treeViewState, _multiColumnHeader) {
            taskController = _taskController;
            index = _index;

            Init(_multiColumnHeader);
            Reload();
            ExpandAll();
        }

        public void Init(MultiColumnHeader _multiColumnHeader) {
            rowHeight = kRowHeights;
            columnIndexForTreeFoldouts = 1;
            showAlternatingRowBackgrounds = true;
            useScrollView = false;
            showBorder = true;
            customFoldoutYOffset = (kRowHeights - EditorGUIUtility.singleLineHeight) * 0.5f; // center foldout in the row since we also center content. See RowGUI
            extraSpaceBeforeIconAndLabel = kToggleWidth;
        }

        // internal count id
        int id = 0;
        // BuildRoot is called every time Reload is called to ensure that TreeViewItems 
        // are created from data. Here we create a fixed set of items. In a real world example,
        // a data model should be passed into the TreeView and the items created from the model.
        protected override TreeViewItem BuildRoot() {

            // This section illustrates that IDs should be unique. The root item is required to 
            // have a depth of -1, and the rest of the items increment from that.
            id = 0;

            TreeViewItem root = new TreeViewItem<Node>(id, -1, taskController.treeRootNodes[index]);
            id++;
            HandleNode(root, taskController.treeRootNodes[index]);

            // Utility method that initializes the TreeViewItem.children and .parent for all items.
            SetupDepthsFromParentsAndChildren(root);

            // Return root of the tree
            return root;
        }

        /// <summary>
        /// Recursively called function to build the TreeView
        /// </summary>
        void HandleNode(TreeViewItem parent, Node currentNode) {
            TreeViewItem treeViewItem = new TreeViewItem<Node>(id, currentNode);
            id++;
            parent.AddChild(treeViewItem);

            if (!(currentNode is RunTreeNode)) {
                if (currentNode.children != null) {
                    foreach (Node childNode in currentNode.children) {
                        HandleNode(treeViewItem, childNode);
                    }
                }
            }
        }

        protected override void RowGUI(RowGUIArgs args) {
            TreeViewItem<Node> item = args.item as TreeViewItem<Node>;
            for (int i = 0; i < args.GetNumVisibleColumns(); ++i) {
                CellGUI(args.GetCellRect(i), item, (ColumnSorting)args.GetColumn(i), ref args);
            }
        }

        void CellGUI(Rect cellRect, TreeViewItem<Node> item, ColumnSorting column, ref RowGUIArgs args) {
            // Center the cell rect vertically using EditorGUIUtility.singleLineHeight.
            // This makes it easier to place controls and icons in the cells.
            CenterRectUsingSingleLineHeight(ref cellRect);

            switch (column) {
                case ColumnSorting.LineNumber:
                    string lineNumberString = String.Format("{0:D4}", item.data.lineNumber);
                    GUI.Label(cellRect, lineNumberString);
                    break;

                case ColumnSorting.Code:
                    // Show a TextField for the data text string
                    float indent = GetContentIndent(item);
                    Rect toggleRect = cellRect;
                    toggleRect.x += indent;
                    toggleRect.width = cellRect.width - indent;

                    GUI.Label(toggleRect, item.data.text);
                    break;

                case ColumnSorting.TaskState:
                    GUI.color = Node.NodeStateColors[item.data.nodeState];
                    GUI.DrawTexture(cellRect, Node.NodeStateIcons[item.data.nodeState], ScaleMode.ScaleToFit);
                    //GUI.Box(cellRect, "");
                    GUI.color = Color.white;
                    //GUI.Label(cellRect, Node.TaskStateDescriptions[item.data.taskState]);
                    break;

                case ColumnSorting.ResultCache:
                    GUI.color = Node.NodeResultColors[item.data.nodeResult];
                    //GUI.Label(cellRect, Node.TaskResultLabels[item.data.resultCache]);
                    GUI.DrawTexture(cellRect, Node.NodeResultIcons[item.data.nodeResult], ScaleMode.ScaleToFit);
                    //GUI.Box(cellRect, "");
                    GUI.color = Color.white;
                    //GUI.Label(cellRect, Node.TaskResultDescriptions[item.data.resultCache]);
                    break;

                case ColumnSorting.Log:
                    item.data.logActive = EditorGUI.Toggle(cellRect, item.data.logActive);
                    break;

                case ColumnSorting.LogResult:
                    if (!String.IsNullOrEmpty(item.data.logString)) {
                        GUI.Label(cellRect, item.data.logString);
                    }
                    break;
            }
        }

        public static MultiColumnHeaderState CreateDefaultMultiColumnHeaderState(float treeViewWidth) {
            bool canSort = false;
            TextAlignment headerTextAlignment = TextAlignment.Left;
            TextAlignment sortingAlignment = TextAlignment.Right;
            bool sortedAscending = false;
            bool autoResize = true;
            bool allowToggleVisibility = false;

            var columns = new[]
            {
            new MultiColumnHeaderState.Column {
                headerContent = new GUIContent("#", "Code Line Number"),
                width = 40,
                minWidth = 10,

                headerTextAlignment = headerTextAlignment,
                sortedAscending = sortedAscending,
                canSort = canSort,
                sortingArrowAlignment = sortingAlignment,
                autoResize = autoResize,
                allowToggleVisibility = allowToggleVisibility
            },

            new MultiColumnHeaderState.Column {
                headerContent = new GUIContent("Code", "The Parsed Code Line"),
                width = 300,
                minWidth = 50,

                headerTextAlignment = headerTextAlignment,
                sortedAscending = sortedAscending,
                canSort = canSort,
                sortingArrowAlignment = sortingAlignment,
                autoResize = autoResize,
                allowToggleVisibility = allowToggleVisibility
            },

            new MultiColumnHeaderState.Column {
                headerContent = new GUIContent(EditorGUIUtility.FindTexture("FilterByLabel"), "..."),
                contextMenuText = "State",
                width = 30,
                minWidth = 30,
                maxWidth = 60,

                headerTextAlignment = headerTextAlignment,
                sortedAscending = sortedAscending,
                canSort = canSort,
                sortingArrowAlignment = sortingAlignment,
                autoResize = autoResize,
                allowToggleVisibility = allowToggleVisibility
            },

            new MultiColumnHeaderState.Column {
                headerContent = new GUIContent(EditorGUIUtility.FindTexture("FilterByType"), "..."),
                contextMenuText = "Result",
                width = 30,
                minWidth = 30,
                maxWidth = 60,

                headerTextAlignment = headerTextAlignment,
                sortedAscending = sortedAscending,
                canSort = canSort,
                sortingArrowAlignment = sortingAlignment,
                autoResize = autoResize,
                allowToggleVisibility = allowToggleVisibility

            },

            new MultiColumnHeaderState.Column {
                headerContent = new GUIContent("Log"),
                width = 28,
                minWidth = 20,

                headerTextAlignment = headerTextAlignment,
                sortedAscending = sortedAscending,
                canSort = canSort,
                sortingArrowAlignment = sortingAlignment,
                autoResize = autoResize,
                allowToggleVisibility = allowToggleVisibility
            },

            new MultiColumnHeaderState.Column {
                headerContent = new GUIContent("LogResult"),
                width = 20,
                minWidth = 20,

                headerTextAlignment = headerTextAlignment,
                sortedAscending = sortedAscending,
                canSort = canSort,
                sortingArrowAlignment = sortingAlignment,
                autoResize = autoResize,
                allowToggleVisibility = allowToggleVisibility
            },
        };

            var state = new MultiColumnHeaderState(columns);
            return state;
        }
    }

    [CustomEditor(typeof(TaskController))]
    public class TaskControllerInspector : Editor {

        // nodes as ordered list which is used to display code lines
        public List<Node> nodeLines;


        [SerializeField] TreeViewState[] m_TreeViewStates = new TreeViewState[0];
        //The TreeView is not serializable, so it should be reconstructed from the tree data.
        TaskTreeView[] m_SimpleTreeViews = new TaskTreeView[0];


        private float timer = 0;
        private int updatesPerSecond = 10;
        public void Update() {
            Debug.Log("TaskControllerInspector.Update");
            timer += Time.deltaTime;
            if (timer > (1f / updatesPerSecond)) {
                // calls OnInspectorGUI every frame
                Debug.Log("TaskControllerInspector.Update: Repaint");
                Repaint();
            }
        }


        public override bool RequiresConstantRepaint() {
            TaskController taskController = (TaskController)target;
            return taskController.RequireUiUpdate();
        }


        public bool uiGenerated {
            get {
                return (nodeLines != null);
            }
        }

        public bool treeGenerated {
            get {
                bool equalSize = (m_SimpleTreeViews.Length == ((TaskController)target).treeRootNodes.Count);
                return equalSize;
            }
        }

        public void GenerateUi(TaskController _taskController) {
            if (!uiGenerated) {
                if (_taskController.debugLog) Debug.Log("TaskControllerInspector.GenerateUi");
                nodeLines = new List<Node>();
                try {
                    foreach (Node treeNode in _taskController.treeRootNodes) {
                        nodeLines.AddRange(treeNode.getAllNodesRecursively(false).OrderBy(o => o.lineNumber).ToList());
                        nodeLines.Add(null);
                    }
                    //nodeLines = _taskController.rootNode.getAllNodesRecursively(true).OrderBy(o => o.lineNumber).ToList();
                }
                catch {
                    // pass
                }
            }
        }

        public void ResetUi(TaskController _taskController) {
            if (_taskController.debugLog) Debug.Log("TaskControllerInspector.Reset");
            nodeLines = null;
            m_TreeViewStates = new TreeViewState[0];
            m_SimpleTreeViews = new TaskTreeView[0];
        }

        public override void OnInspectorGUI() {
            //Debug.Log("TaskControllerInspector.OnInspectorGUI");
            TaskController _taskController = (TaskController)target;

            DrawDefaultInspector();
            DrawCustomInspector(_taskController);

            if (_taskController.generationError) {
                GUI.contentColor = Color.red;
                GUILayout.TextArea("ERROR GENERATING CODE, BUILD MANUALLY");
                //GUI.Label(Rect(400, 200, 200, 60), "color");
                GUI.contentColor = Color.black;
            }

            //DrawCodeInspector(_taskController);

            if (!treeGenerated) {
                if (_taskController.debugLog) Debug.Log("Generate Trees");
                int treeRootNodeCount = _taskController.treeRootNodes.Count;

                // Check whether there is already a serialized view state (state 
                // that survived assembly reloading)
                if (m_TreeViewStates.Length == 0) {
                    m_TreeViewStates = new TreeViewState[treeRootNodeCount];
                }
                for (int i = 0; i < m_TreeViewStates.Length; i++) {
                    m_TreeViewStates[i] = new TreeViewState();
                }


                if (m_SimpleTreeViews.Length == 0) {
                    m_SimpleTreeViews = new TaskTreeView[treeRootNodeCount];
                }
                for (int i = 0; i < m_SimpleTreeViews.Length; i++) {
                    MultiColumnHeaderState state = TaskTreeView.CreateDefaultMultiColumnHeaderState(EditorGUIUtility.currentViewWidth);
                    MultiColumnHeader header = new MultiColumnHeader(state);
                    m_SimpleTreeViews[i] = new TaskTreeView(m_TreeViewStates[i], header, _taskController, i);
                }
            }

            if (treeGenerated) {
                for (int i = 0; i < m_SimpleTreeViews.Length; i++) {
                    Rect rect = EditorGUILayout.GetControlRect(false, m_SimpleTreeViews[i].totalHeight);

                    m_SimpleTreeViews[i].OnGUI(rect);
                }
            }
        }

        public void DrawCustomInspector(TaskController _taskController) {

            GUILayout.BeginHorizontal();
            bool regenerateCode = GUILayout.Button("Regenerate Code");
            GUILayout.TextArea(" " + _taskController.generatedSize + " ");
            GUILayout.EndHorizontal();

            if (!_taskController.hasScript) {
                return;
            }

            // if not generated, call reset code view and 
            if ((!_taskController.isGenerated) || regenerateCode) {
                ResetUi(_taskController);
                _taskController.ResetData();
            }


            if (!_taskController.isGenerated) {
                _taskController.Generate();
            }


            if (!uiGenerated) {
                GenerateUi(_taskController);
            }

        }

        public void DrawCodeInspector(TaskController _taskController) {
            //Debug.Log("TaskController.DrawCodeInspector");
            if (_taskController.isGenerated && uiGenerated) {
                // if reset dont run editor ui update
                if (nodeLines != null) {
                    int lastLineNumber = 0;
                    int foldAbove = int.MaxValue;
                    foreach (Node n in nodeLines) {
                        if (n == null) {
                            GUILayout.Space(20);
                        }
                        else {
                            //if ((n.lineNumber - lastLineNumber) > 2) {
                            //	GUILayout.Space(20);
                            //}
                            lastLineNumber = n.lineNumber;

                            // reset fold above value when dropping to same level again
                            if (n.nodeDepth <= foldAbove) {
                                foldAbove = int.MaxValue;
                            }
                            // if fold value active
                            if (n.nodeDepth <= foldAbove) {
                                // reset fold above value when dropping to same level again
                                foldAbove = int.MaxValue;

                                string lineNumber = String.Format("{0:D4}", n.lineNumber);

                                // draw layout
                                GUILayout.BeginHorizontal();

                                GUI.color = Color.white;
                                EditorGUILayout.LabelField(lineNumber, GUILayout.Width(34));

                                //GUILayout.Space(7);
                                //EditorGUI.indentLevel = n.nodeDepth;
                                //GUILayout.Space(15 * n.nodeDepth + 10);

                                //n.isFolded = !EditorGUILayout.Foldout(!n.isFolded, GUIContent.none);
                                //if (n.isFolded) {
                                //	foldAbove = n.nodeDepth;
                                //}
                                //GUILayout.Space(15 * n.nodeDepth);
                                //EditorGUILayout.LabelField(n.codeLine, GUILayout.ExpandWidth(true));
                                //GUILayout.Space(10);

                                // draw line number
                                //GUI.color = Color.white;
                                //switch (n.logActive) {
                                //	case true:
                                //		GUI.color = Color.red;
                                //		break;
                                //	case false:
                                //		GUI.color = Color.white;
                                //		break;
                                //}
                                //if (GUILayout.Button(lineNumber, GUILayout.Width(38))) {
                                //	n.logActive = !n.logActive;
                                //}

                                // draw folding
                                string foldLabel = n.isFolded ? "+" : "-";
                                GUI.color = n.isFolded ? Color.yellow : Color.white;
                                if (GUILayout.Button(foldLabel, GUILayout.Width(18))) {
                                    n.isFolded = !n.isFolded;
                                }
                                if (n.isFolded) {
                                    foldAbove = n.nodeDepth;
                                }


                                // draw node result
                                switch (n.logActive) {
                                    case true:
                                        GUI.color = Color.red;
                                        break;
                                    case false:
                                        GUI.color = Color.white;
                                        break;
                                }
                                if (GUILayout.Button("log", GUILayout.Width(30))) {
                                    n.logActive = !n.logActive;
                                }

                                // code line indention
                                GUILayout.Space(10 * n.nodeDepth);

                                // draw task state
                                switch (n.nodeState) {
                                    case NodeState.NotRunning:
                                        GUI.color = Color.yellow;
                                        break;
                                    case NodeState.Running:
                                    case NodeState.FirstRun:
                                        GUI.color = Color.green;
                                        break;
                                    case NodeState.Aborting:
                                        GUI.color = Color.red;
                                        break;
                                }
                                //EditorGUILayout.LabelField(Node.TaskStateLabels[n.taskState], GUILayout.Width(18));
                                if (GUILayout.Button(Node.NodeStateLabels[n.nodeState], GUILayout.Width(24))) {
                                }


                                // draw node result
                                GUI.color = Node.NodeResultColors[n.nodeResult];
                                //EditorGUILayout.LabelField(Node.TaskReturnLabels[n.resultCache], GUILayout.Width(18));
                                if (GUILayout.Button(Node.NodeResultLabels[n.nodeResult], GUILayout.Width(24))) {
                                }

                                // draw code line
                                GUI.color = Color.white;
                                GUILayout.TextField(n.codeLine);

                                // ---
                                GUILayout.EndHorizontal();
                            }
                        }
                    }
                }
            }
        }
    }
#endif

    // remembers the last active TaskController, so a task can access it directly without a reference to the TaskController
    public class SingletonTaskController : MonoBehaviour {
        static SingletonTaskController _instance = null;
        public static SingletonTaskController Instance {
            get {
                if (_instance != null) {
                    return _instance;
                }
                if (_instance == null) {
                    _instance = Resources.FindObjectsOfTypeAll<SingletonTaskController>().FirstOrDefault();
                    if (_instance == null) {
                        _instance = new GameObject("TaskController", typeof(SingletonTaskController)).GetComponent<SingletonTaskController>();
                    }
                }
                return _instance;
            }
        }

        public TaskController current;
    }

    public class TaskController : MonoBehaviour {

        public UnityEngine.TextAsset taskScriptZwo;
        public bool readUnityComponents = false;
        public bool debugLog = false;
        public bool nodeConsoleDebugActive = false;

        public bool generationError = false;

        //[HideInInspector]
        //public TaskImplementationController taskImplementationController;
        //[HideInInspector]
        //public TaskScriptParser taskScriptParser;

        // list of the tasks
        [HideInInspector]
        public Dictionary<string, Dictionary<string, TaskImplementation>> tasks = new Dictionary<string, Dictionary<string, TaskImplementation>>();
        // the contents of the script
        [HideInInspector]
        public string code = "";
        // the tokens are generated by the TokenEnvironment
        [HideInInspector]
        public List<Token> tokens = new List<Token>();
        // the treenodes are generated by the NodeGenerator
        [HideInInspector]
        public List<Node> treeRootNodes = new List<Node>();

        [HideInInspector]
        public TreeNode rootNode;

        // used in actions to access the node
        //[HideInInspector]
        //public Task currentTask;
        [HideInInspector]
        private Node currentNode;

        private bool anyNodeChanged = false;

        public bool hasScript {
            get {
                return (taskScriptZwo != null);
            }
        }

        public bool isLoaded {
            get {
                return (!string.IsNullOrEmpty(code)) && hasScript;
            }
        }

        public bool isGenerated {
            get {
                return (rootNode != null) && isLoaded;
            }
        }

        public int generatedSize {
            get {
                if (isGenerated) {
                    return rootNode.getAllNodesRecursively(true).OrderBy(o => o.lineNumber).Count<Node>();
                }
                return 0;
            }
        }


        private void Start() {
            Generate();
        }

        public void Generate() {
            if (!isGenerated && !generationError) {
                if (debugLog) Debug.Log("TaskController.Generate");

                ResetData();

                //Debug.Log("TaskController.Start:  ------------------------ TaskImplmentations ------------------------");
                TaskImplementationController taskImplementationController = new TaskImplementationController(this, readUnityComponents, debugLog);
                taskImplementationController.CheckComponents(gameObject);
                // read the contents of the script, remove tabs and spaces
                code = taskScriptZwo.text; //.Replace("\t", " "); //.Replace(" ", "");

                //Debug.Log("TaskController.Start: ------------------------ Tokenizer ------------------------");
                // generate the tokens
                Tokenizer tokenizer = new Tokenizer(this);
                tokenizer.Tokenize();

                //Debug.Log("TaskController.Start: ------------------------ NodeGenerator ------------------------");
                // generate the node structure
                NodeGenerator nodeGenerator = new NodeGenerator(this);
                bool nodeGenSuccess = nodeGenerator.Generate();
                generationError = generationError || !nodeGenSuccess;

                //Debug.Log("TaskController.Start: ------------------------ NodeBinder ------------------------");
                NodeBinder nodeActionBinder = new NodeBinder(this);
                bool bindSuccess = nodeActionBinder.Bind();
                generationError = generationError || !bindSuccess;

                //Debug.Log("TaskController.Start: ------------------------ Run ------------------------");
                rootNode = GetRootNode();
            }
        }

        /// <summary>
        /// Reset the whole Task / Node / Action Tree structure for regeneration from the source behaviour tree.
        /// </summary>
        public void ResetData() {
            if (isGenerated) {
                if (debugLog) Debug.Log("TaskController.Reset");
                currentNode = null;

                if (rootNode != null) {
                    foreach (Node treeNode in treeRootNodes) {
                        foreach (Node node in treeNode.getAllNodesRecursively(false)) {
                            node.Reset();
                        }
                        //nodeLines.AddRange(treeNode.getAllNodesRecursively(false).OrderBy(o => o.lineNumber).ToList());
                        //nodeLines.Add(null);
                    }
                    rootNode = null;
                }
                treeRootNodes = new List<Node>();

                if (tokens.Count > 0) {
                    foreach (Token t in tokens) {
                        t.Reset();
                    }
                    tokens = new List<Token>();
                }

                if (tasks != null) {
                    //taskImplementationController.Reset();
                    tasks = null;
                }

                code = "";
            }

            generationError = false;
        }

        public void SetUiUpdate() {
            anyNodeChanged = true;
        }
        public bool RequireUiUpdate() {
            bool tmp = anyNodeChanged;
            anyNodeChanged = false;
            return tmp;
        }

        NodeResult rootResult = NodeResult.Succeeded;
        public void Update() {
            if (rootNode != null) {
                // run every update
                rootResult = rootNode.Tick(NodeExecute.Run, 0);
                if (rootResult != NodeResult.Running) {
                    if (debugLog) {
                        Debug.Log("TaskController.Update: root run resulted in " + rootResult + " restarting");
                    }
                    rootResult = rootNode.Tick(NodeExecute.Abort, 0);
                    rootResult = NodeResult.Running;
                }
            }
            else {
                Debug.LogError("TaskController.Update: rootNode is null");
            }
        }

        public void SetCurrentNode(Node value) {
            currentNode = value;
            SingletonTaskController.Instance.current = this;
        }
        public Node GetCurrentNode() {
            return currentNode;
        }

        public TreeNode GetRootNode() {
            foreach (Node node in treeRootNodes) {
                TreeNode treeNode = node as TreeNode;
                if (treeNode.name == "Root") {
                    return treeNode;
                }
            }
            Debug.LogError("TaskController.GetRootNode: failed no root node found");
            return null;
        }
    }
}