﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyBT {
    public class CommentNode : Node {
        public string comment;

        public object userData;
        public string debugData;

        public CommentNode(TaskController _taskController, int _nodeDepth, int[] _tokenIndexes, string _comment)
            : base(_taskController, _nodeDepth, _tokenIndexes) {
            comment = _comment;
        }

        /// <summary>
        /// reset the whole behaviour tree structure
        /// </summary>
        public override void Reset() {
            base.Reset();
            userData = null;
            debugData = "";
            comment = "";
        }

        public override string text {
            get {
                return spacedText;
            }
        }
    }
}