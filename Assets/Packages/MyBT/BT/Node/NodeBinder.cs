﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MyBT {
    public class NodeBinder {
        TaskController taskController;
        public bool debug = false;
        public NodeBinder(TaskController _taskController) {
            taskController = _taskController;
        }

        public void Reset() {
            taskController = null;
        }

        public bool Bind() {
            bool success = true;
            foreach (Node treeNode in taskController.treeRootNodes) {
                foreach (Node node in treeNode.getAllNodesRecursively(false).OrderBy(o => o.lineNumber).ToList()) {
                    success = success && Bind(node);
                }
            }
            return success;
        }

        public bool Bind(Node currentNode) {
            bool success = true;
            if (currentNode != null) {
                // based on current parent node:
                switch (Node.NodeTypeDict[currentNode.GetType()]) {
                    case Node.TaskNodeType.RunTreeNode:
                        // this node
                        RunTreeNode runTreeNode = currentNode as RunTreeNode;
                        if (debug)
                            Debug.Log("NodeBinder.BindRecursive: searching for " + runTreeNode.runTreeNodeName);
                        // iterate all parent tree nodes that we have
                        foreach (Node node in taskController.treeRootNodes) {
                            TreeNode treeNode = node as TreeNode;
                            if (debug)
                                Debug.Log("NodeBinder.BindRecursive: checking " + treeNode.name);
                            // if the current node name matches the runTreeNode name, store it
                            if (runTreeNode.runTreeNodeName == treeNode.name) {
                                //runTreeNode.runNode = treeNode;
                                runTreeNode.Add(treeNode);
                            }
                        }

                        if (runTreeNode.children.Count != 1) {
                            Debug.LogError("NodeBinder.BindRecursive: invalid child count on RunTreeNode named: '" + runTreeNode.runTreeNodeName + "' childcount: '" + runTreeNode.children.Count + "' (can also be duplicate Tree)");
                            // Debug.LogError("NodeBinder.BindRecursive: THIS SHOULD DELETE THE WHOLE ACTION TREE!!!!");
                            success = false;
                        }
                        else {
                            if (debug) {
                                Debug.Log("NodeBinder.BindRecursive: bound " + runTreeNode.runTreeNodeName + " with " + runTreeNode.runNode + " " + ((runTreeNode.runNode != null) ? runTreeNode.runNode.name : "null"));
                            }
                        }
                        break;
                    
                    case Node.TaskNodeType.ActionNode:
                        // using this 
                        ActionNode actionNode = currentNode as ActionNode;

                        if (actionNode != null) {
                            string functionName = actionNode.functionName;
                            //List<object> functionParameters = actionNode.paramters;
                            TaskParameterGroup taskParameterGroup = actionNode.taskParameterGroup;

                            Dictionary<string, Dictionary<string, TaskImplementation>> taskParameterImplementationDict = taskController.tasks;

                            if (taskParameterImplementationDict.ContainsKey(functionName)) {
                                TaskImplementation implementation = NodeBinder.FindMatchingImplementation(taskParameterImplementationDict[functionName], taskParameterGroup);
                                if (implementation != null) {
                                    Action action;
                                    implementation.GetAction(taskParameterGroup, out action);

                                    // Debug.Log("TaskScriptParser.RunScript: accepted action matching " + functionName + "  " + implementation);
                                    actionNode.action = action;
                                }
                                else {
                                    Debug.LogError("TaskScriptParser.RunScript: no matching implementation found for " + functionName + "  " + taskParameterGroup);
                                    success = false;
                                }
                                //taskParameterImplementationDict[functionName].ExecuteTask(taskParameters);
                            }
                            else {
                                Debug.LogError("TaskScriptParser.RunScript: No Function Found " + functionName + " available are (" + string.Join(", ", taskParameterImplementationDict.Keys) + ")");
                                success = false;
                            }

                        }
                        else {
                            Debug.LogError("BindRecursive.");
                        }

                        break;
                    
                    case Node.TaskNodeType.TaskNode:
                    case Node.TaskNodeType.TreeNode:
                    case Node.TaskNodeType.DecoratorNode:
                    case Node.TaskNodeType.CompositeNode:
                    case Node.TaskNodeType.CommentNode:
                        // no binding of those nodes implemented or required
                        break;
                }
            }
            return success;
        }

        public static TaskImplementation FindMatchingImplementation(Dictionary<string, TaskImplementation> taskImplementations, TaskParameterGroup taskParameterGroup) {
            string taskParmStr = taskParameterGroup.ToString();

            // if the parameters match exactly
            if (taskImplementations.ContainsKey(taskParmStr)) {
                return taskImplementations[taskParmStr];
            }

            // more complex implementation of finding the right implementation for the given parameters
            else {
                // use this for getting the right one
                // https://stackoverflow.com/questions/3631547/select-right-generic-method-with-reflection
                // maybe just omit this and let it do without checking myself, but having the automatic system do it...?

                //Debug.LogWarning("TaskScriptParser.FindMatchingImplementation: no matching implementation found for " + taskParameterGroup);
                foreach (KeyValuePair<string, TaskImplementation> kvp in taskImplementations) {
                    if (kvp.Value.parameters.CheckCompatiblity(taskParameterGroup)) {
                        // Debug.Log("TaskScriptParser.FindMatchingImplementation: possible implementations " + kvp.Key + " accepted " + kvp.Value);
                        return kvp.Value;
                    }
                    //else {
                    //	Debug.Log("TaskScriptParser.FindMatchingImplementation: possible implementations " + kvp.Key + " ignored " + kvp.Value);
                    //}
                }
            }

            // get some implementation, all will have the same name
            string functionName = taskImplementations[taskImplementations.Keys.First()].fullName;
            Debug.Log("TaskScriptParser.FindMatchingImplementation: no matching implementation found for " + functionName + " : " + taskParmStr);
            return null;
        }
    }
}