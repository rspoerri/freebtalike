﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MyBT;

public class SimpleController : MonoBehaviour {
	public string identifier = "";
	private Vector3 target;
	public bool debug = true;
	public Transform targetBeacon;

	[Task]
	public void SetRandomTarget (float x, float y, float range) {
		if (Task.isRunning) {
			Vector2 r = Random.insideUnitCircle * range;
			target = new Vector3(x + r.x, 0, y + r.y);
			if (targetBeacon != null) {
				targetBeacon.position = target;
			}
			Task.SetSucceed();
		}
	}

	[Task]
	public void MoveToTarget() {
		MoveBy(target.x, target.y, target.z);
	}

	[Task]
	public void MoveBy(float x, float y, float z) {
		if (Task.isRunning) {
			transform.position += new Vector3(x, y, z) * Time.deltaTime;
			if (debug)
				Debug.Log("TestScript.Move: " + identifier + " : " + x + " " + y + " " + z + " " + Task.getState);
		}
	}

	[Task]
	public void MoveByTimed(float x, float y, float z, float timer) {
		switch (Task.getState) {
			case TaskState.FirstRun:
				Task.SetData(this, Time.time);
				break;
			case TaskState.Running:
				float startTime = (float)Task.GetData(this);
				float elapsed = Time.time - startTime;
				if (elapsed > timer) {
					Task.SetSucceed();
				}
				transform.position += new Vector3(x, y, z) * Time.deltaTime;
				break;
			case TaskState.Aborting:
				break;
		}
		if (debug)
			Debug.Log("TestScript.MoveTime: " + identifier + " : " + x + " " + y + " " + z + " " + " " + timer + " " + Task.getState);
	}

	[Task]
	public void GetKeyDown(KeyCode keyCode) {
		if (Task.isRunning) {
			if (Input.GetKeyDown(keyCode))
				Task.SetSucceeded();
			else
				Task.SetFailed();
		}
	}

	[Task]
	public void DebugBreak() {
		//throw new System.ArgumentException("Absichtlicher Error Zum Testen", "original");
		if (Task.isRunning) {
			Debug.Break();
			Task.SetSucceeded();
		}
	}

	[Task]
	public void RotateTowards(float x, float y, float z, float rotSpeed) {
		if (Task.isRunning) {
			Vector3 positionOffset = new Vector3(x,y,z) - transform.position;
			Quaternion targetRotation = Quaternion.LookRotation(positionOffset);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * rotSpeed);
			float angleOffset = Quaternion.Angle(transform.rotation, targetRotation);
		}
	}

	[Task]
	public void RotateTowards (float rotSpeed) {
		if (Task.isRunning) {
			RotateTowards(target.x, target.y, target.z, rotSpeed);
		}
	}

	[Task]
	public void RotateTowards(string target, float rotSpeed) {
		if (Task.isRunning) {
			Transform targetTransform = GameObject.Find(target).transform;
			RotateTowards(targetTransform.position.x, targetTransform.position.y, targetTransform.position.z, rotSpeed);
		}
	}

    [Task]
    public void RotateAway(float rotSpeed) {
        if (Task.isRunning) {
            RotateAway(target.x, target.y, target.z, rotSpeed);
        }
    }

    [Task]
    public void RotateAway(string target, float rotSpeed) {
        if (Task.isRunning) {
            Transform targetTransform = GameObject.Find(target).transform;
            RotateAway(targetTransform.position.x, targetTransform.position.y, targetTransform.position.z, rotSpeed);
        }
    }

    [Task]
	public void RotateAway(float x, float y, float z, float rotationSpeed) {
		if (Task.isRunning) {
			Vector3 positionOffset = transform.position - new Vector3(x,y,z);
			Quaternion targetRotation = Quaternion.LookRotation(positionOffset);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
			float angleOffset = Quaternion.Angle(transform.rotation, targetRotation);
		}
	}

	[Task]
	public void MoveTowards(string target, float velocity) {
		if (Task.isRunning) {
			Transform targetTransform = GameObject.Find(target).transform;
			transform.position = Vector3.MoveTowards(transform.position, targetTransform.position, Time.deltaTime * velocity);
			if ((targetTransform.position - transform.position).magnitude < 0.3f) {
				Task.SetSucceeded(this);
			}
		}
	}

	[Task]
	public void MoveForward(float velocity) {
		if (Task.isRunning) {
			transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward, Time.deltaTime * velocity);
		}
	}

	[Task]
	public void MoveAway(string target, float velocity) {
		if (Task.isRunning) {
			Transform targetTransform = GameObject.Find(target).transform;
			Vector3 awayVector = transform.position - targetTransform.position;
			transform.position = Vector3.MoveTowards(transform.position, transform.position + awayVector.normalized, Time.deltaTime * velocity);
			if ((targetTransform.position - transform.position).magnitude < 0.3f) {
				Task.SetFailed(this);
			}
		}
	}

	[Task]
	public void HasDistance(string target, float distance) {
		if (Task.isRunning) {
			Transform targetTransform = GameObject.Find(target).transform;
			float currentDistance = (targetTransform.position - transform.position).magnitude;
			if (debug)
				Debug.Log("TestScript.HasDistance: distance " + currentDistance);
			if (currentDistance > distance) {
				Task.SetSucceeded();
			}
		}
	}

	[Task]
	public void Avoid(string target, float distance) {
		if (Task.isRunning) {
			Transform targetTransform = GameObject.Find(target).transform;
			float currentDistance = (targetTransform.position - transform.position).magnitude;
			if (debug)
				Debug.Log("TestScript.Avoid: distance " + currentDistance);
			if (currentDistance < distance) {
				Task.SetFailed();
			}
		}
	}

	[Task]
	public void Timer(float timer) {
        if (Task.isDebugging)
            Debug.Log("TestScript.Timer: start " + timer + " " + Task.getState);

        switch (Task.getState) {
			case TaskState.FirstRun:
				Task.data = Time.time;
                goto case TaskState.Running;
			case TaskState.Running:
				float startTime = (float)Task.data;
				float elapsed = Time.time - startTime;
				Task.log = "" + timer;
				if (elapsed > timer) {
					Task.SetSucceeded();
				}
				break;
			case TaskState.Aborting:
				break;
		}

        if (Task.isDebugging)
            Debug.Log("TestScript.Timer: end " + timer + " " + Task.getState);
	}

	[Task]
	public void NearTarget(float x, float y, float z, float distance) {
		if (Task.isRunning) {
			float currentDistance = (new Vector3(x,y,z) - transform.position).magnitude;
			if (debug) Debug.Log("TestScript.HasDistance: distance " + currentDistance);
			if (currentDistance < distance) {
				Task.SetSucceeded();
			}
		}
	}

	[Task]
	public void NearTarget (float distance) {
		if (Task.isRunning) {
			NearTarget(target.x, target.y, target.z, distance);
		}
	}

	[Task]
	public void NearTarget (string target, float distance) {
		if (Task.isRunning) {
			Transform targetTransform = GameObject.Find(target).transform;
			NearTarget(targetTransform.position.x, targetTransform.position.y, targetTransform.position.z, distance);
		}
	}

}
