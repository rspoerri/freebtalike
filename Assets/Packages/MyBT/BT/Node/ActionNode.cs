﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyBT {
    public enum Actions {
        Idle,
        Log,
        RestartBehaviourTree,
        StartBehaviourTree,
        StopBehaviourTree,
        Wait
    }


    public class ActionNode : Node {
        public string functionName;
        public TaskParameterGroup taskParameterGroup;

        public Action action;

        public object userData;
        public string debugData;

        public TaskState taskState = TaskState.NotRunning;
        public TaskResult taskResult = TaskResult.Running;

        public ActionNode(TaskController _taskController, int _nodeDepth, int[] _tokenIndexes, string _functionName, TaskParameterGroup _taskParameterGroup) : base(_taskController, _nodeDepth, _tokenIndexes) {
            functionName = _functionName;
            taskParameterGroup = _taskParameterGroup;
        }

        /// <summary>
        /// reset the whole behaviour tree structure
        /// </summary>
        public override void Reset() {
            base.Reset();
            userData = null;
            debugData = "";
            action = null;
            taskParameterGroup = null;
            functionName = "";
        }

        public override NodeResult Tick(NodeExecute nodeExecute, int recursions = 0) {
            base.PreTick(nodeExecute);

            // nodeResult = base.Tick(nodeExecute, recursions);

            if (action != null) {
                switch (nodeExecute) {
                    case NodeExecute.Run:
                        switch (taskState) {
                            case TaskState.NotRunning:
                                taskState = TaskState.FirstRun;
                                break;
                            case TaskState.FirstRun:
                                taskState = TaskState.Running;
                                break;
                            case TaskState.Running:
                                taskState = TaskState.Running;
                                break;
                            case TaskState.Aborting:
                                taskState = TaskState.FirstRun;
                                break;
                        }
                        break;

                    case NodeExecute.Abort:
                        switch (taskState) {
                            case TaskState.NotRunning:
                                taskState = TaskState.NotRunning;
                                break;
                            case TaskState.FirstRun:
                                taskState = TaskState.Aborting;
                                break;
                            case TaskState.Running:
                                taskState = TaskState.Aborting;
                                break;
                            case TaskState.Aborting:
                                // is not required to be called
                                taskState = TaskState.Aborting;
                                break;
                        }
                        break;
                }

                if (taskState == TaskState.FirstRun) {
                    taskResult = TaskResult.Running;
                    // nodeResult = NodeResult.Running;
                    userData = null;
                    debugData = "";
                }

                if (taskState != TaskState.NotRunning) {
                    // give task to controller to return to function
                    // can change taskResult
                    taskController.SetCurrentNode(this);
                    // run action
                    action();
                }
                // if not running, set result to running, as this is required for the continuation in the next loop
                else {
                    taskResult = TaskResult.Running;
                }

                // set default action result
                nodeResult = NodeResult.Running;
                switch (taskResult) {
                    case TaskResult.Running:
                        nodeResult = NodeResult.Running;
                        taskState = TaskState.Running;
                        break;
                    case TaskResult.Succeeded:
                        nodeResult = NodeResult.Succeeded;
                        taskState = TaskState.NotRunning;
                        break;
                    case TaskResult.Failed:
                        nodeResult = NodeResult.Failed;
                        taskState = TaskState.NotRunning;
                        break;
                    case TaskResult.Error:
                        nodeResult = NodeResult.Error;
                        taskState = TaskState.Aborting;
                        break;
                }

                // log
                if (debugActive) {
                    Debug.Log ( 
                        NodeLogger("HandleExecute", " nodeExecute=" + nodeExecute + " taskState=" + taskState + " taskResult=" + nodeResult , recursions));
                }

                base.PostTick(nodeResult);

                // finish the nodes
                return nodeResult;
            }

            if (debugActive)
                Debug.Log(NodeLogger("HandleExecute", " " + nodeExecute, recursions));

            Debug.LogError("Execute." + this + " : Action not found!");
            taskState = TaskState.FirstRun;
            nodeResult = NodeResult.Error;
            base.PostTick(nodeResult);
            return nodeResult;
        }
    }
}